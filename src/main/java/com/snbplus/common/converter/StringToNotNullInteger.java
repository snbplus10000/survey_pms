package com.snbplus.common.converter;

import org.springframework.core.convert.converter.Converter;

public class StringToNotNullInteger implements Converter<String, Integer> {
	public Integer convert(String valText) {
		if (valText == null || valText.equals("")) 
			return 0;
		else 
			return Integer.valueOf(valText);
	}
}