package com.snbplus.util;

public class LoginInfo {
	private int id;
	private String loginId;
	private String name;
	private String state;
	private String msg;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getLoginId() {
		return loginId;
	}

	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	@Override
	public String toString() {
		return "LoginInfo [id=" + id + ", loginId=" + loginId + ", name=" + name + ", state=" + state + ", msg=" + msg
				+ "]";
	}

}
