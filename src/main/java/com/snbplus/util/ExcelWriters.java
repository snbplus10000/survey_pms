package com.snbplus.util;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;

import kr.or.koms.file.web.FileDownloadController;


public class ExcelWriters {
	
	
	private FileOutputStream out;
	private XSSFWorkbook wb;
	private XSSFSheet s;
	private String fileName;
	private String sheetName;
	private String createpath;
	private XSSFRow r;
	private XSSFCell c;
	
	/**
	 * 기본 생성자
	 */
	public ExcelWriters(String createpath) throws Exception{
		fileName = RandomStringUtils.randomAlphanumeric(25) + ".xlsx";
		sheetName = "Sheet1";
		this.createpath = createpath;
		out = new FileOutputStream(createpath + fileName);
		wb = new XSSFWorkbook();
		s = wb.createSheet();
		wb.setSheetName(0, sheetName);
	}
	
	
	/**
	 * 로우 생성
	 * 
	 * @param index
	 * @return
	 */
	public boolean createRow(int index) throws Exception{
		if(s == null){
			return false;
		}else{
			r = null;
			r = s.createRow(index);
		}
		
		return true;
	}


	/**
	 * 로우 반환
	 * 
	 * @param index
	 * @return
	 */
	public boolean getRow(int index) throws Exception{
		if(s == null){
			return false;
		}else{
			r = null;
			r = s.getRow(index);
			if(r == null){
				r = s.createRow(index);
			}
		}
		
		return true;
	}
	
	/**
	 * 셀생성
	 * 
	 * @param index
	 * @return
	 */
	public boolean createCell(int index) throws Exception{
		if(r == null){
			return false;
		}else{
			c = null;
			c = r.createCell(index);
		}
		return true;
	}
	
	/**
	 * 문자값 입력
	 * 
	 * @param value
	 * @return
	 */
	public boolean setCellString(String value) throws Exception{
		if(c == null){
			return false;
		}else{
			c.setCellValue(value);
		}
		return true;
	}
	
	/**
	 * 숫자값입력
	 * 
	 * @param value
	 * @return
	 */
	public boolean setCellNumeric(int value) throws Exception{
		if(c == null){
			return false;
		}else{
			c.setCellValue(value);
		}
		return true;
	}
	
	/**
	 * 숫자값입력
	 * 
	 * @param value
	 * @return
	 */
	public boolean setCellNumeric(long value) throws Exception{
		if(c == null){
			return false;
		}else{
			c.setCellValue(value);
		}
		return true;
	}
	
	/**
	 * 날짜값 입력
	 * 
	 * @param value
	 * @return
	 */
	public boolean setCellDate(String value) throws Exception{
		if(c == null){
			return false;
		}else{
			c.setCellValue(value);
		}
		return true;
	}
	
	public void setTitleCell() throws Exception{
		if(wb != null && c != null && s != null){
			XSSFCellStyle cs = this.getCellStyle();
			cs.setBorderTop(BorderStyle.THIN);
			cs.setBorderRight(BorderStyle.THIN);
			cs.setBorderLeft(BorderStyle.THIN);
			cs.setBorderBottom(BorderStyle.THIN);
			cs.setFillForegroundColor(IndexedColors.SEA_GREEN.index);
			cs.setFillPattern(FillPatternType.SOLID_FOREGROUND);
			cs.setAlignment(HorizontalAlignment.CENTER);
			cs.setVerticalAlignment(VerticalAlignment.CENTER);
			XSSFFont f = wb.createFont();
			f.setBold(true);
			cs.setFont(f);
			c.setCellStyle(cs);
		}
	}
	
	/**
	 * 셀 보더
	 * 
	 * @throws Exception
	 */
	public void setBorderCell() throws Exception{
		if(wb != null && c != null && s != null){
			XSSFCellStyle cs = this.getCellStyle();
			cs.setBorderTop(BorderStyle.THIN);
			cs.setBorderRight(BorderStyle.THIN);
			cs.setBorderLeft(BorderStyle.THIN);
			cs.setBorderBottom(BorderStyle.THIN);
			cs.setFillForegroundColor(IndexedColors.WHITE.index);
			cs.setFillPattern(FillPatternType.SOLID_FOREGROUND);
			cs.setVerticalAlignment(VerticalAlignment.CENTER);
			XSSFFont f = wb.createFont();
			f.setBold(false);
			cs.setFont(f);
			c.setCellStyle(cs);
			
			
		}
	}
	public void setColumnWidth(int idx) throws Exception{
		if(s != null){
			s.autoSizeColumn(idx);
			s.setColumnWidth(idx, (s.getColumnWidth(idx))+512);
		}
	}
	
	/**
	 * 스타일 셋
	 * 
	 * @return
	 */
	public XSSFCellStyle getCellStyle() throws Exception{
		XSSFCellStyle cs = null;

		if(wb != null &&c != null){
			cs = wb.createCellStyle();
		}
		
		return cs;
	}
	
	/**
	 * 엑셀 저장
	 * 
	 * @return
	 */
	public boolean completeExcel() throws Exception{
		try{
			if(wb == null || out == null){
				return false;
			}else{
				wb.write(out);
				out.close();
			}
		}catch(Exception e){
			return false;
		}finally{
			if(out != null){try{out.close();}catch(Exception e){}}
		}
		return true;
	}
	
	/**
	 * 파일다운로드
	 * 
	 * @param response
	 * @param saveName
	 * @return
	 */
	public void download(String saveName, HttpServletResponse response, HttpServletRequest request) throws Exception{
		FileDownloadController fileDownload = new FileDownloadController();
		fileDownload.excelDownload(createpath, fileName, saveName, response, request);
		
		/**
		 * 생성파일 삭제
		 */
		java.io.File delFile = new java.io.File(createpath, fileName); 
		if(delFile.exists()){
			delFile.delete();
		}
		
	}
	
}
