package com.snbplus.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

public class AbstractMessageController {

	protected Logger logger = LoggerFactory.getLogger(getClass());

	@Autowired
	protected MsgSource msgSource;

	public final String getMsg(String code) {
		return msgSource.getMsg(code);
	}
	
}
