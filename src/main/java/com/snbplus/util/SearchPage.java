package com.snbplus.util;

public class SearchPage {
	private final static int DEFAULT_SIZE = 10;
	
	private int page=1;
	private int pageSize=10;
	private int offset;
	private String dept;
	private String status;
	private String cate;
	private String srchCode;
	private String srchValue;
	private String order;
	private int memberId;
	
	public SearchPage(){
		
	}
	
	public SearchPage(int page) {
		this(page, DEFAULT_SIZE);
	}
	
	public SearchPage(int page, int pageSize) {
		this.page = page;
		this.pageSize = pageSize;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public int getOffset() {
		if(offset == 0) {
			this.offset = this.pageSize * (this.page-1);
		}
		return offset;
	}

	public void setOffset(int offset) {
		this.offset = offset;
	}

	public String getDept() {
		return dept;
	}

	public void setDept(String dept) {
		this.dept = dept;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getCate() {
		return cate;
	}

	public void setCate(String cate) {
		this.cate = cate;
	}

	public int getMemberId() {
		return memberId;
	}

	public void setMemberId(int memberId) {
		this.memberId = memberId;
	}

	public String getSrchCode() {
		return srchCode;
	}

	public void setSrchCode(String srchCode) {
		this.srchCode = srchCode;
	}

	public String getSrchValue() {
		return srchValue;
	}

	public void setSrchValue(String srchValue) {
		this.srchValue = srchValue;
	}

	public String getOrder() {
		return order;
	}

	public void setOrder(String order) {
		this.order = order;
	}
	

	
}
