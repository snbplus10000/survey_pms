package com.snbplus.util;

import java.util.Locale;

import org.springframework.context.support.ReloadableResourceBundleMessageSource;

public class MsgSource {

	private ReloadableResourceBundleMessageSource reloadableResourceBundleMessageSource;

	public void setReloadableResourceBundleMessageSource(ReloadableResourceBundleMessageSource reloadableResourceBundleMessageSource) {
		this.reloadableResourceBundleMessageSource = reloadableResourceBundleMessageSource;
	}
	
	public ReloadableResourceBundleMessageSource getReloadableResourceBundleMessageSource() {
		return reloadableResourceBundleMessageSource;
	}
	
	public String getMsg(String code) {
		//Locale locale = Locale.getDefault();
		Locale locale = new Locale("en", "US");
		return getReloadableResourceBundleMessageSource().getMessage(code, null, locale);
	}
}
