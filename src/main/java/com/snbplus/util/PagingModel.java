package com.snbplus.util;

public class PagingModel {

	private static final long serialVersionUID = 2187677459087526219L;

	private int pageNo; // 현재 페이지
	private int groupNo; // 현재 그룹번호
	private int totalRecord; // 총 개수
	private int totalPage; // 총 페이지 수
	private int totalGroup;

	private int recordEndNo; // 마지막 글번호
	private int recordStartNo; // 첫번째 글번호

	private int pagePerRecordCnt; // 페이지당 개수
	private int groupPerPageCnt; // 페이지당 그룹 수

	private int pageEno; // 현재 그룹 끝 번호
	private int pageSno; // 현재 그룹 시작 번호

	private int prevPageNo; // 이전 페이지 번호
	private int nextPageNo; // 다음 페이지 번호
	
	// 검색
	private String srchCode;
	private String srchValue;

	public PagingModel(int pageNo, int total) {
		this(pageNo, total, 10, 10);
	}

	public PagingModel(int pageNo, int total, int pagePerRecordCnt, int groupPerPageCnt) {
		this.pageNo = pageNo;
		this.totalRecord = total;
		this.pagePerRecordCnt = pagePerRecordCnt;
		this.groupPerPageCnt = groupPerPageCnt;

		this.recordEndNo = this.pageNo * this.pagePerRecordCnt;
		this.recordStartNo = this.recordEndNo - (this.pagePerRecordCnt - 1);
		if (this.recordEndNo > this.totalRecord) {
			this.recordEndNo = this.totalRecord;
		}

		this.totalPage = this.totalRecord / this.pagePerRecordCnt
				+ (this.totalRecord % this.pagePerRecordCnt > 0 ? 1 : 0);

		this.totalGroup = this.totalPage / this.groupPerPageCnt + (this.totalPage % this.groupPerPageCnt > 0 ? 1 : 0);

		if (this.pageNo > this.totalPage) {
			this.pageNo = this.totalPage;
		}

		this.groupNo = this.pageNo / this.groupPerPageCnt + (this.pageNo % this.groupPerPageCnt > 0 ? 1 : 0);

		this.pageEno = this.groupNo * this.groupPerPageCnt;
		this.pageSno = this.pageEno - (this.groupPerPageCnt - 1);

		if (this.pageEno > this.totalPage) {
			this.pageEno = this.totalPage;
		}

		if (this.pageSno < 1) {
			this.pageSno = 1;
		}
		
		if (this.pageEno < 1) {
			this.pageEno = 1;
		}
		
		if (this.totalPage < 1) {
			this.totalPage = 1;
		}
		
		//this.prevPageNo = this.pageSno - this.groupPerPageCnt;
		this.prevPageNo = this.pageSno - 1;
		this.nextPageNo = this.pageSno + this.groupPerPageCnt;

		if (this.prevPageNo < 1) {
			this.prevPageNo = 1;
		}
		if (this.nextPageNo > this.totalPage) {
			this.nextPageNo = this.totalPage / this.groupPerPageCnt * this.groupPerPageCnt + 1;
		}
	}

	public int getPageNo() {
		return pageNo;
	}

	public void setPageNo(int pageNo) {
		this.pageNo = pageNo;
	}

	public int getGroupNo() {
		return groupNo;
	}

	public void setGroupNo(int groupNo) {
		this.groupNo = groupNo;
	}

	public int getTotalRecord() {
		return totalRecord;
	}

	public void setTotalRecord(int totalRecord) {
		this.totalRecord = totalRecord;
	}

	public int getTotalPage() {
		return totalPage;
	}

	public void setTotalPage(int totalPage) {
		this.totalPage = totalPage;
	}

	public int getTotalGroup() {
		return totalGroup;
	}

	public void setTotalGroup(int totalGroup) {
		this.totalGroup = totalGroup;
	}

	public int getRecordEndNo() {
		return recordEndNo;
	}

	public void setRecordEndNo(int recordEndNo) {
		this.recordEndNo = recordEndNo;
	}

	public int getRecordStartNo() {
		return recordStartNo;
	}

	public void setRecordStartNo(int recordStartNo) {
		this.recordStartNo = recordStartNo;
	}

	public int getPagePerRecordCnt() {
		return pagePerRecordCnt;
	}

	public void setPagePerRecordCnt(int pagePerRecordCnt) {
		this.pagePerRecordCnt = pagePerRecordCnt;
	}

	public int getGroupPerPageCnt() {
		return groupPerPageCnt;
	}

	public void setGroupPerPageCnt(int groupPerPageCnt) {
		this.groupPerPageCnt = groupPerPageCnt;
	}

	public int getPageEno() {
		return pageEno;
	}

	public void setPageEno(int pageEno) {
		this.pageEno = pageEno;
	}

	public int getPageSno() {
		return pageSno;
	}

	public void setPageSno(int pageSno) {
		this.pageSno = pageSno;
	}

	public int getPrevPageNo() {
		return prevPageNo;
	}

	public void setPrevPageNo(int prevPageNo) {
		this.prevPageNo = prevPageNo;
	}

	public int getNextPageNo() {
		return nextPageNo;
	}

	public void setNextPageNo(int nextPageNo) {
		this.nextPageNo = nextPageNo;
	}

	public String getSrchCode() {
		return srchCode;
	}

	public void setSrchCode(String srchCode) {
		this.srchCode = srchCode;
	}

	public String getSrchValue() {
		return srchValue;
	}

	public void setSrchValue(String srchValue) {
		this.srchValue = srchValue;
	}

	@Override
	public String toString() {
		return "PagingModel [pageNo=" + pageNo + ", groupNo=" + groupNo + ", totalRecord=" + totalRecord
				+ ", totalPage=" + totalPage + ", totalGroup=" + totalGroup + ", recordEndNo=" + recordEndNo
				+ ", recordStartNo=" + recordStartNo + ", pagePerRecordCnt=" + pagePerRecordCnt + ", groupPerPageCnt="
				+ groupPerPageCnt + ", pageEno=" + pageEno + ", pageSno=" + pageSno + ", prevPageNo=" + prevPageNo
				+ ", nextPageNo=" + nextPageNo + "]";
	}

}