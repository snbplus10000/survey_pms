package com.snbplus.util;

import java.io.UnsupportedEncodingException;
import java.util.Base64;
import java.util.Base64.Decoder;
import java.util.Base64.Encoder;

public class EncryptUtil {
	
	public static String base64Encoding(String str) throws UnsupportedEncodingException {
		byte[] target = str.getBytes("UTF-8");
		
		Encoder encoder = Base64.getEncoder();
		
		String result = encoder.encodeToString(target);
		
		return result;
	}

	public static String base64Decoding(String str) throws UnsupportedEncodingException {
		
		Decoder decoder = Base64.getDecoder();
		
		byte[] resultByte = decoder.decode(str);
		
		String result = new String(resultByte, "UTF-8");
		
		return result;
	}
}
