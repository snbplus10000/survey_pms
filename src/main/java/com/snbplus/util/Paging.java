package com.snbplus.util;

public class Paging {
	 private int totalCount;
	    private int page = 1;
	    private int pageSize = 10;
	    private int pageCount = 10;
	    private int offset;
	    private int totalPages;
	    private int startPage;
	    private int endPage;
	    
	    private int prevPage;
	    private int nextPage;
	    
	  
	    private String srchCode;
	    private String srchValue;
	    private String srchTerm;
	    
	   
	    private String condition1; //異붽�議곌굔1
	    private String condition2; //異붽�議곌굔2
	    private String condition3; 
	    private String condition4; //異붽�議곌굔4
	    
		public int getPage() {
			return page;
		}

		public void setPage(int page) {
			this.page = page;
		}

		public int getPageSize() {
			return pageSize;
		}

		public void setPageSize(int pageSize) {
			this.pageSize = pageSize;
		}

		public int getPageCount() {
			return pageCount;
		}

		public void setPageCount(int pageCount) {
			this.pageCount = pageCount;
		}

		public int getOffset() {
			return offset;
		}

		public void setOffset(int offset) {
			this.offset = offset;
		}

		public int getTotalPages() {
			return totalPages;
		}

		public int getStartPage() {
			return startPage;
		}

		public void setStartPage(int startPage) {
			this.startPage = startPage;
		}

		public int getEndPage() {
			return endPage;
		}

		public void setEndPage(int endPage) {
			this.endPage = endPage;
		}

		public void setTotalPages(int totalPages) {
			this.totalPages = totalPages;
		}

		public int getTotalCount() {
			return totalCount;
		}

		public int getPrevPage() {
			return prevPage;
		}

		public void setPrevPage(int prevPage) {
			this.prevPage = prevPage;
		}

		public int getNextPage() {
			return nextPage;
		}

		public void setNextPage(int nextPage) {
			this.nextPage = nextPage;
		}
		
	    public void setTotalCount(int totalCount) {
	        this.totalCount = totalCount;
	        totalPages = totalCount / pageSize;
	        if (totalCount % pageSize > 0) {
	            totalPages++;
	        }
	        if (totalPages == 0) {
	            totalPages = 1;
	        }
	        
	        //offset = (page-1) * pageCount;
	        //offset = pageSize;
	        offset = (page-1) * pageSize;
	
	        startPage = ((page-1)/10) * 10 +1;
	        endPage = startPage + 10-1;
	        if(endPage > totalPages){
	        	endPage = totalPages;
	        }
	        
	        prevPage = (page - 1) != 0 ? page - 1 : 1;
	        nextPage = (page + 1) < totalPages ? page + 1 : totalPages;
	    }
	    
	 
	    private int limit; 
	    private String boardDiv; 

		public int getLimit() {
			return limit;
		}

		public void setLimit(int limit) {
			this.limit = limit;
		}

		public String getBoardDiv() {
			return boardDiv;
		}

		public void setBoardDiv(String boardDiv) {
			this.boardDiv = boardDiv;
		}

		public String getSrchCode() {
			return srchCode;
		}

		public void setSrchCode(String srchCode) {
			this.srchCode = srchCode;
		}

		public String getSrchValue() {
			return srchValue;
		}

		public void setSrchValue(String srchValue) {
			this.srchValue = srchValue;
		}
		
		public String getSrchTerm() {
			return srchTerm;
		}

		public void setSrchTerm(String srchTerm) {
			this.srchTerm = srchTerm;
		}

		public String getCondition1() {
			return condition1;
		}

		public void setCondition1(String condition1) {
			this.condition1 = condition1;
		}

		public String getCondition2() {
			return condition2;
		}

		public void setCondition2(String condition2) {
			this.condition2 = condition2;
		}

		public String getCondition3() {
			return condition3;
		}

		public void setCondition3(String condition3) {
			this.condition3 = condition3;
		}

		public String getCondition4() {
			return condition4;
		}

		public void setCondition4(String condition4) {
			this.condition4 = condition4;
		}
}
