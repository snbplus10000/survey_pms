package com.snbplus.util;

import java.io.FileOutputStream;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.io.FilenameUtils;

import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.html.simpleparser.HTMLWorker;
import com.itextpdf.text.html.simpleparser.StyleSheet;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.draw.LineSeparator;

public class ConvertHTMLToPDF {
	public String[] pdfbu(String content,String image) throws UnsupportedEncodingException {
		//String file = URLEncoder.encode("c://"+filename+".pdf","UTF-8");         
		String fileId = StringUtil.getUUIDByTimeBased().replace("-", "");
		String file = "/home/mma/apache-tomcat-8.0.46/webapps/snbplus/pdf/"+fileId+".pdf";
//		String file ="c://"+fileId+".pdf";
		
		
		PdfWriter pdfWriter = null;

		try {
			System.out.println("file::"+file+"image::"+image);
			// create a new document                      오, 왼, 위,아래
			Document document = new Document(PageSize.A4, 20, 20, 30, 30);

			// get Instance of the PDFWriter
			pdfWriter = PdfWriter.getInstance(document, new FileOutputStream(file));

//			document.setPageSize(PageSize.A4);

			document.open();

			HTMLWorker htmlWorker = new HTMLWorker(document);

			HashMap<String, Object> interfaceProps = new HashMap<String, Object>();

			StyleSheet styles = new StyleSheet();

//			DefaultFontProvider dfp = new DefaultFontProvider("c:/malgun.ttf");
			
			DefaultFontProvider dfp = new DefaultFontProvider("http://220.73.136.147/public/css/malgun.ttf");
			dfp.getFont("malgun", 30);		
			

			// 폰트 파일 설정 (한글 나오게 하기 위해 설정 필요함
			interfaceProps.put(HTMLWorker.FONT_PROVIDER, dfp);

			StringBuffer sb = new StringBuffer();
			sb.append(
					"<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">");

			sb.append("<html xmlns=\"http://www.w3.org/1999/xhtml\">");

			sb.append("<html>");

			sb.append("<head>");

			sb.append("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />");
			
			sb.append("</head>");

			sb.append("<body>");

			sb.append("<table border='0'>");

			sb.append("  <tr>");
			sb.append("    <td width='6%' ><img src='http://220.73.136.147/public/img/letter/"+image+".jpg'/></td>");
			sb.append("    <td valign='top' style='padding-top:8%;'><br/><br/><br/><br/><br/><br/><br/><br/><br/><div style='line-height:31px'>");
			sb.append(		 content+"</div></td>");
			sb.append("    <td width='6%' ></td>");
			sb.append("  </tr>");
			sb.append("  <tr>");
			sb.append("    <td colspan='3' height='20' ></td>");	
			sb.append("  </tr>");
			sb.append("</table>");


			

			sb.append("</body>");

			sb.append("</html>");

			LineSeparator ls = new LineSeparator(); 
			ls.setPercentage(50);

			StringReader strReader = new StringReader(sb.toString());
			List<Element> objects = htmlWorker.parseToList(strReader, styles, interfaceProps);

			for (int k = 0; k < objects.size(); ++k) {

				document.add((Element) objects.get(k));
			}
			document.close();
			// close the writer
			pdfWriter.close();
			
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		String[] result = new String[2];
		result[0] = file;
		result[1] = fileId+".pdf";

		return result;
	}

}
