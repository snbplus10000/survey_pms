package com.snbplus.util;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;

import org.apache.commons.io.FileUtils;

public class FileUtil {
	public static final String ACCEPT_EXT = ".pdf"
			+ ",.jpeg,.jpg,.png,.gif,.bmp,.tiff"
			+ ",.txt,.doc"
			+ ",.hwp"
			+ ",.ppt,.pptx"
			+ ",.xlsx,.xls"
			+ ",.zip,.7z,.rar,.alz";
	public static boolean createThumbnail(File loadFile, File saveFile, int maxDim) throws IOException {
		FileInputStream fis = new FileInputStream(loadFile);
		BufferedImage im = ImageIO.read(fis);
		Image inImage = new ImageIcon(loadFile.getPath()).getImage();
		double scale = (double) maxDim / (double) inImage.getHeight(null);
		
		if (inImage.getWidth(null) > inImage.getHeight(null)) {
			scale = (double) maxDim / (double) inImage.getWidth(null);
		}
		
		int scaledW = (int) (scale * inImage.getWidth(null));
		int scaledH = (int) (scale * inImage.getHeight(null));
		
		BufferedImage thumb = new BufferedImage(scaledW, scaledH, BufferedImage.TYPE_INT_RGB);
		Graphics2D g2 = thumb.createGraphics();
		g2.drawImage(im, 0, 0, scaledW, scaledH, null);
		
		fis.close();
		inImage.flush(); // flush()를 해야한다.
		
		return ImageIO.write(thumb, "jpg", saveFile);
	}
	
	public static boolean moveToFile(File orgFile, File moveFile) throws IOException {
		byte[] buf = new byte[1024];
		FileInputStream fin = null;
		FileOutputStream fout = null;

		if(!moveFile.getParentFile().exists()) {
			moveFile.getParentFile().mkdirs();
		}
	    if(orgFile.exists()) {
	    	System.out.println("is file :" + orgFile.exists());
	        if(!orgFile.renameTo(moveFile)){
	            buf = new byte[1024];
	            fin = new FileInputStream(orgFile);
	            fout = new FileOutputStream(moveFile);
	         
	            int read = 0;
	            while((read=fin.read(buf,0,buf.length))!=-1){
	                fout.write(buf, 0, read);
	            }
	             
	            fin.close();
	            fout.close();
	            orgFile.delete();
	        }

	    }
		return false;
	}

	public static void deleteDir(String dirPath) {
		File file = new File(dirPath);
		String[] fnameList = file.list();
		int fCnt = fnameList.length;
		String childPath = "";

		for (int i = 0; i < fCnt; i++) {
			childPath = dirPath + "/" + fnameList[i];
			File f = new File(childPath);
			if (!f.isDirectory()) {
				f.delete(); // 파일이면 바로 삭제
			} else {
				deleteDir(childPath);
			}
		}
	}
}
