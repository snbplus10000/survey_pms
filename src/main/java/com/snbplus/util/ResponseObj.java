package com.snbplus.util;

import com.google.gson.Gson;

public class ResponseObj {
	private String result = "";
	private String msg = "";
	private Object json;
	private Object value;
	private Object page;
	private Object auth;
	private Object menu;

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public Object getJson() {
		return json;
	}

	public void setJson(Object json) {
		this.json = json;
	}

	public Object getValue() {
		return value;
	}

	public void setValue(Object value) {
		this.value = value;
	}

	public Object getPage() {
		return page;
	}

	public void setPage(Object page) {
		this.page = page;
	}

	public Object getAuth() {
		return auth;
	}

	public void setAuth(Object auth) {
		this.auth = auth;
	}

	public Object getMenu() {
		return menu;
	}

	public void setMenu(Object menu) {
		this.menu = menu;
	}

	public String toJson() {
		Gson gson = new Gson();

		return gson.toJson(this);
	}
}
