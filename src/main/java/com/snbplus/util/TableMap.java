package com.snbplus.util;

import org.apache.commons.collections.map.ListOrderedMap;

public class TableMap extends ListOrderedMap {

    private static final long serialVersionUID = 6723434363565852261L;

    @Override
    public Object put(Object key, Object value) {
        return super.put(StringUtil.convert2CamelCase((String) key), value);
    }
    
    public String getString(Object key) {
    	String result = "";
    	if(super.get(key) != null) {
    		result = super.get(key).toString();
    	}
    	return result;
    }
    
    public int getInt(Object key) {
    	int result = 0;
    	if(super.get(key) != null) {
    		result = Integer.parseInt(super.get(key).toString());
    	}
    	return result;
    }
    
    public long getLong(Object key) {
    	long result = 0;
    	if(super.get(key) != null) {
    		result = Long.parseLong(super.get(key).toString());
    	}
    	return result;
    }
    
    public Double doubleNull2Zero(Object key) {
    	double result = 0.0;
    	if(super.get(key) != null) {
    		result = Double.parseDouble(super.get(key).toString());
    	}
    	return result;
    }

}