package com.snbplus.util;

/**
 * Created by Administrator on 2017-04-04.
 */

public class SMSParser {

    static RuleSet ruleSet;
    static Lexer lexer;
    static {
        ruleSet = new RuleSet();
        ruleSet.appendRule(new Rule("header","\\[Web발신\\]"));
        ruleSet.appendRule(new Rule("header","\\(Web발신\\)")); 
        ruleSet.appendRule(new Rule("header","체크카드출금"));
        ruleSet.appendRule(new Rule("moneyTotal","누적\\s*([\\d,]+)원"));
        ruleSet.appendRule(new Rule("moneyTotal","누계\\s*([\\d,]+)원"));
        ruleSet.appendRule(new Rule("moneyTotal","누적-[\\d,\\-]+원"));
        ruleSet.appendRule(new Rule("moneyTotal","누적:([\\d,]+)원"));
        ruleSet.appendRule(new Rule("moneyTotal","잔액[\\d,\\-]+원?"));
        ruleSet.appendRule(new Rule("money","([\\d,]+)원([^/\\s]+)"));
        ruleSet.appendRule(new Rule("money","([\\d,]+)원"));
        ruleSet.appendRule(new Rule("money","USD([\\d,\\.]+)"));
        //ruleSet.appendRule(new Rule("CARD","\\S+\\([\\d\\*]{4}\\)"));
        //ruleSet.appendRule(new Rule("CARD","\\p{IsHangul}+\\([\\d\\*]{4}\\)"));
        ruleSet.appendRule(new Rule("card","\\S+은행"));
        ruleSet.appendRule(new Rule("card","KEB하나"));
        ruleSet.appendRule(new Rule("type","거절"));
        ruleSet.appendRule(new Rule("type","출금")); // 04.21 추가
        
        ruleSet.appendRule(new Rule("name","\\p{IsHangul}+\\s*님")); // 04.19 추가

        ruleSet.appendRule(new Rule("card","롯데\\s")); // 04.24
        ruleSet.appendRule(new Rule("card","\\S+카드"));
        ruleSet.appendRule(new Rule("card","\\S+체크"));
        ruleSet.appendRule(new Rule("card","\\S+법인"));
        
        ruleSet.appendRule(new Rule("card","\\S+농협기업")); // 04.19 추가
        
        ruleSet.appendRule(new Rule("date","\\d\\d/\\d\\d"));
        ruleSet.appendRule(new Rule("time","\\d\\d:\\d\\d"));
        ruleSet.appendRule(new Rule("name","[\\p{IsHangul}\\*]{2,4}님"));
        
        ruleSet.appendRule(new Rule("shop","李가네갈비")); // 04.21 추가
        ruleSet.appendRule(new Rule("shop","\\(주\\)\\p{IsHangul}+"));
        ruleSet.appendRule(new Rule("shop","\\p{IsHangul}+\\(주\\)"));
        
        ruleSet.appendRule(new Rule("shop","\\p{IsHangul}+\\s*주식")); // 04.17 추가
        ruleSet.appendRule(new Rule("shop","[0-9A-Za-z\\p{IsHangul}]+\\_모바일")); // 04.17 추가
        
        ruleSet.appendRule(new Rule("shop","주식회사\\p{IsHangul}+"));
        ruleSet.appendRule(new Rule("shop","[A-Za-z\\p{IsHangul}]+\\s+칼국수"));  // 04.21 추가
        ruleSet.appendRule(new Rule("shop","[A-Za-z\\p{IsHangul}]+\\s+이"));  // 04.21 추가
        ruleSet.appendRule(new Rule("shop","[A-Za-z\\p{IsHangul}]+\\s+쌍문제일점"));  // 04.21 추가
        ruleSet.appendRule(new Rule("shop","[A-Za-z\\p{IsHangul}]+\\s+중구청"));  // 04.21 추가
        ruleSet.appendRule(new Rule("shop","[A-Za-z\\p{IsHangul}]+\\s+신당한양점"));  // 04.21 추가
        ruleSet.appendRule(new Rule("shop","[A-Za-z\\p{IsHangul}]+\\s+떡볶이"));  // 04.21 추가
        ruleSet.appendRule(new Rule("shop","[A-Za-z\\p{IsHangul}]+\\s+한옥마을"));  // 04.21 추가
        ruleSet.appendRule(new Rule("shop","[A-Za-z\\p{IsHangul}]+\\s+김포"));  // 04.21 추가
        ruleSet.appendRule(new Rule("shop","[A-Za-z\\p{IsHangul}]+\\_\\p{IsHangul}+"));  // 04.21 추가
        ruleSet.appendRule(new Rule("shop","[0-9A-Za-z\\p{IsHangul}]+"));
        ruleSet.appendRule(new Rule("type"," (일시불)"));
        ruleSet.appendRule(new Rule("type"," 일시불"));
        ruleSet.appendRule(new Rule("type"," 사용"));
        ruleSet.appendRule(new Rule("type"," 취소"));
        ruleSet.appendRule(new Rule("type","-승인"));
        lexer = new Lexer(ruleSet);
    }

    public static void parse(String sms, Rule.OnMatchListener listener){
    	listener.mapClear();
    	lexer.lex(sms, listener);
    }
}
