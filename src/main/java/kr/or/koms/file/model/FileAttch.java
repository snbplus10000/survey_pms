package kr.or.koms.file.model;

import java.util.Date;

public class FileAttch {

	private int id;
	private int refId;
	private String refTable;
	private String fileName;
	private String orgFileName;
	private String path;
	private long size;
	private String ext;
	private int sort;
	private Date created;

	private boolean isNew = false;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getRefId() {
		return refId;
	}

	public void setRefId(int refId) {
		this.refId = refId;
	}

	public String getRefTable() {
		return refTable;
	}

	public void setRefTable(String refTable) {
		this.refTable = refTable;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getOrgFileName() {
		return orgFileName;
	}

	public void setOrgFileName(String orgFileName) {
		this.orgFileName = orgFileName;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public long getSize() {
		return size;
	}

	public void setSize(long size) {
		this.size = size;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public String getExt() {
		return ext;
	}

	public void setExt(String ext) {
		this.ext = ext;
	}

	public int getSort() {
		return sort;
	}

	public void setSort(int sort) {
		this.sort = sort;
	}

	public boolean isNew() {
		return isNew;
	}

	public void setNew(boolean isNew) {
		this.isNew = isNew;
	}

	@Override
	public String toString() {
		return "FileAttch [id=" + id + ", refId=" + refId + ", fileName=" + fileName + ", orgFileName=" + orgFileName
				+ ", path=" + path + ", size=" + size + ", ext=" + ext + ", sort=" + sort + ", created=" + created
				+ ", isNew=" + isNew + "]";
	}

}
