package kr.or.koms.file.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.snbplus.util.TableMap;

import kr.or.koms.file.dao.IFileAttchDao;
import kr.or.koms.file.model.FileAttch;

@Service
public class FileService implements IFileService {

	@Autowired
	private IFileAttchDao fileAttchDao;
	
	public FileAttch selectById(int id) {
		return fileAttchDao.selectById(id);
	}
	
	
	public int delete(int id) {
		return fileAttchDao.delete(id);
	}
	
	public List<FileAttch> getListByRefId(List<TableMap> list){
		
		List<FileAttch> fileList = new ArrayList<FileAttch>();
		
		for(int i=0; i < list.size(); i++){
			int idx =list.get(i).getInt("id");
			
			FileAttch fileAttch = new FileAttch();
			//fileAttch = fileAttchDao.selectByRefId(idx);
			fileList.add(fileAttch);
		}
		
		return fileList;
	}


}
