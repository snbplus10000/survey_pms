package kr.or.koms.file.service;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.snbplus.util.TableMap;

import kr.or.koms.file.model.FileAttch;



public interface IFileService {

	@Transactional
	public FileAttch selectById(int id);
	
	@Transactional
	public List<FileAttch> getListByRefId(List<TableMap> list);
	
	@Transactional
	public int delete(int id);
	
	
}
