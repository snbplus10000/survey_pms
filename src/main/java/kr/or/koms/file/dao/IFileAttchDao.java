package kr.or.koms.file.dao;

import kr.or.koms.file.model.FileAttch;

public interface IFileAttchDao {
	
	public FileAttch selectByRefId(int id);
	
	
	public FileAttch selectByRef(FileAttch fileAttch);
	
//	public int deleteRefId(int id);

	public int deleteRef(FileAttch fileAttch);
//	
//	// DEFAULT
//	public List<FileAttch> selectAll();
//	
	public FileAttch selectById(int id);
	
	public int insert(FileAttch fileAttch);
	
//	public int update(FileAttch fileAttch);
//	
	public int delete(int id);
	
}