package kr.or.koms.file.web;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URLEncoder;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import kr.or.koms.file.model.FileAttch;
import kr.or.koms.file.service.IFileService;



@Controller
@RequestMapping("/file")
public class FileDownloadController {
	private static Logger logger = LoggerFactory.getLogger(FileDownloadController.class); 

    @Autowired private IFileService fileService;
    @Value("${Globals.filePath}") private String filePath;
    //@Autowired private FileSystemResource uploadDirResource;
	/*@Value("${Globals.fileStorePath}")
	private String fileHomeDir;*/

	/**
	 * 브라우저 구분 얻기.
	 * 
	 * @param request
	 * @return
	 */
	private String getBrowser(HttpServletRequest request) {
		String header = request.getHeader("User-Agent");
		if (header.indexOf("MSIE") > -1) {
			return "MSIE";
		} else if (header.indexOf("Chrome") > -1) {
			return "Chrome";
		} else if (header.indexOf("Opera") > -1) {
			return "Opera";
		} else if (header.indexOf("Trident") > -1) {
			return "MSIE";
		}
		return "Firefox";
	}
	
	/**
	 * Disposition 지정하기.
	 * 
	 * @param filename
	 * @param request
	 * @param response
	 * @throws Exception
	 */
	private void setDisposition(String filename, HttpServletRequest request, HttpServletResponse response) throws Exception {
		String browser = getBrowser(request);
		
		String dispositionPrefix = "attachment; filename=";
		String encodedFilename = null;

		if (browser.equals("MSIE")) {
			encodedFilename = URLEncoder.encode(filename, "UTF-8").replaceAll("\\+", "%20");
		} else if (browser.equals("Firefox")) {
			encodedFilename = "\"" + new String(filename.getBytes("UTF-8"), "8859_1") + "\";";
		} else if (browser.equals("Opera")) {
			encodedFilename = "\"" + new String(filename.getBytes("UTF-8"), "8859_1") + "\";";
		} else if (browser.equals("Chrome")) {
			encodedFilename = "\"" + URLEncoder.encode(filename, "UTF-8") + "\";";

			StringBuffer sb = new StringBuffer();
			sb.append("\"");
			for (int i = 0; i < filename.length(); i++) {
				char c = filename.charAt(i);
				if (c > '~') {
					sb.append(URLEncoder.encode("" + c, "UTF-8"));
				} else {
					sb.append(c);
				}
			}
			sb.append("\";");
			encodedFilename = sb.toString();

		} else {
			// throw new RuntimeException("Not supported browser");
			throw new IOException("Not supported browser");
		}

		response.setHeader("Content-Disposition", dispositionPrefix + encodedFilename);

		if ("Opera".equals(browser)) {
			response.setContentType("application/octet-stream;charset=UTF-8");
		}
	}

	/**
	 * 첨부파일로 등록된 파일에 대하여 다운로드를 제공한다.
	 * 
	 * @param commandMap
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping(value = "/down/{id}", method = RequestMethod.GET)
	public void fileDownload(@PathVariable("id") int id, HttpServletRequest request, HttpServletResponse response) throws Exception {
		// Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
		// if (isAuthenticated) {
//		String fileHomeDir;
//		fileHomeDir = EgovProperties.getProperty(WebAttrKey.FILE_STORE_DIR);

		FileAttch fileAttch = this.fileService.selectById(id);

		File uFile = new File(filePath+fileAttch.getPath() + File.separator + fileAttch.getFileName());

		int fSize = (int) uFile.length();
		if (fSize > 0) {
			String mimetype = "application/x-download";

			// response.setBufferSize(fSize); // OutOfMemeory 발생
			response.setContentType(mimetype);
			// response.setHeader("Content-Disposition", "attachment; filename=\"" + URLEncoder.encode(fvo.getOrignlFileNm(), "utf-8") + "\"");
			setDisposition(fileAttch.getOrgFileName(), request, response);
			response.setContentLength(fSize);

			/*
			 * FileCopyUtils.copy(in, response.getOutputStream()); in.close(); response.getOutputStream().flush(); response.getOutputStream().close();
			 */
			BufferedInputStream in = null;
			BufferedOutputStream out = null;

			try {
				in = new BufferedInputStream(new FileInputStream(uFile));
				out = new BufferedOutputStream(response.getOutputStream());

				FileCopyUtils.copy(in, out);
				out.flush();
			} catch (Exception ex) {
				// ex.printStackTrace();
				// 다음 Exception 무시 처리
				// Connection reset by peer: socket write error
				logger.debug("IGNORED: " + ex.getMessage());
			} finally {
				if (in != null) {
					try {
						in.close();
					} catch (Exception ignore) {
						logger.debug("IGNORED: " + ignore.getMessage());
					}
				}
				if (out != null) {
					try {
						out.close();
					} catch (Exception ignore) {
						logger.debug("IGNORED: " + ignore.getMessage());
					}
				}
			}

		} else {
			response.setContentType("text/html");

			PrintWriter printwriter = response.getWriter();
			printwriter.println("<script>"
					+ "alert('could not get file name : \\n "+ fileAttch.getOrgFileName() + "');"
					+ "window.close();"
					+ "</script>");
			printwriter.flush();
			printwriter.close();
		}
	}
	
	

	/**
	 * 엑셀 다운로드
	 * 
	 * @throws Exception
	 */
	public void excelDownload(String createpath, String fileName, String saveName, HttpServletResponse response, HttpServletRequest request) throws Exception {
		
		File uFile = new File(createpath + fileName);

		int fSize = (int) uFile.length();
		if (fSize > 0) {
			String mimetype = "application/vnd.ms-excel";

			response.setContentType(mimetype);
			// response.setHeader("Content-Disposition", "attachment; filename=\"" + URLEncoder.encode(fvo.getOrignlFileNm(), "utf-8") + "\"");
			setDisposition(saveName, request, response);
			response.setContentLength(fSize);

			/*
			 * FileCopyUtils.copy(in, response.getOutputStream()); in.close(); response.getOutputStream().flush(); response.getOutputStream().close();
			 */
			BufferedInputStream in = null;
			BufferedOutputStream out = null;

			try {
				in = new BufferedInputStream(new FileInputStream(uFile));
				out = new BufferedOutputStream(response.getOutputStream());

				FileCopyUtils.copy(in, out);
				out.flush();
			} catch (Exception ex) {
				// ex.printStackTrace();
				// 다음 Exception 무시 처리
				// Connection reset by peer: socket write error
				logger.debug("IGNORED: " + ex.getMessage());
			} finally {
				if (in != null) {
					try {
						in.close();
					} catch (Exception ignore) {
						logger.debug("IGNORED: " + ignore.getMessage());
					}
				}
				if (out != null) {
					try {
						out.close();
					} catch (Exception ignore) {
						logger.debug("IGNORED: " + ignore.getMessage());
					}
				}
			}

		} else {
			response.setContentType("text/html");

			PrintWriter printwriter = response.getWriter();
			printwriter.println("<script>"
					+ "alert('오류가 발생했습니다.');"
					+ "window.close();"
					+ "</script>");
			printwriter.flush();
			printwriter.close();
		}
	}
	
	
	
	public void fileDownloadCheck(File uFile, HttpServletRequest request, HttpServletResponse response) throws Exception {
		int fSize = (int) uFile.length();

		if (fSize > 0) {
			String mimetype = "application/x-download";

			// response.setBufferSize(fSize); // OutOfMemeory 발생
			response.setContentType(mimetype);
			// response.setHeader("Content-Disposition", "attachment; filename=\"" + URLEncoder.encode(fvo.getOrignlFileNm(), "utf-8") + "\"");
			setDisposition(uFile.getName(), request, response);
			response.setContentLength(fSize);

			/*
			 * FileCopyUtils.copy(in, response.getOutputStream()); in.close(); response.getOutputStream().flush(); response.getOutputStream().close();
			 */
			BufferedInputStream in = null;
			BufferedOutputStream out = null;

			try {
				in = new BufferedInputStream(new FileInputStream(uFile));
				out = new BufferedOutputStream(response.getOutputStream());

				FileCopyUtils.copy(in, out);
				out.flush();
			} catch (Exception ex) {
				// ex.printStackTrace();
				// 다음 Exception 무시 처리
				// Connection reset by peer: socket write error
				logger.debug("IGNORED: " + ex.getMessage());
			} finally {
				if (in != null) {
					try {
						in.close();
					} catch (Exception ignore) {
						logger.debug("IGNORED: " + ignore.getMessage());
					}
				}
				if (out != null) {
					try {
						out.close();
					} catch (Exception ignore) {
						logger.debug("IGNORED: " + ignore.getMessage());
					}
				}
			}

		} else {
			response.setContentType("text/html");

			PrintWriter printwriter = response.getWriter();
			printwriter.println("<script>"
					+ "alert('could not get file name : \\n "+ uFile.getName() + "');"
					+ "window.close();"
					+ "</script>");
			printwriter.flush();
			printwriter.close();
		}
		uFile.delete();
	}
}
