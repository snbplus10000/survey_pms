package kr.or.koms.file.web;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Calendar;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.snbplus.util.AbstractMessageController;
import com.snbplus.util.FileUtil;
import com.snbplus.util.ResponseObj;
import com.snbplus.util.StringUtil;

import kr.or.koms.board.service.IBoardService;
import kr.or.koms.file.model.FileAttch;
import kr.or.koms.file.service.IFileService;

/**
 * Handles requests for the application file upload requests
 */

@RestController
@RequestMapping("/upload")
public class FileUploadController extends AbstractMessageController {

	@Autowired
	private IBoardService boardService;
	
	@Autowired
	private IFileService fileService;
	private static final Logger logger = LoggerFactory.getLogger(FileUploadController.class);
	
	@Value("${Globals.filePath}") private String filePath;
	@Autowired private FileSystemResource tmpDirResource;
	@Autowired private FileSystemResource boardDirResource;
	@Autowired private FileSystemResource boardImageDirResource;
	
	@RequestMapping(value = "/file", method = RequestMethod.POST)
	public ResponseObj upload(@RequestParam("ufile") MultipartFile ufile,
			HttpServletRequest request, HttpSession session) {
		
		ResponseObj res = new ResponseObj();
		FileAttch fileAttch;
	
			String filename = ufile.getOriginalFilename();
			if (!ufile.isEmpty()) {
				
				try {
					String ext = FilenameUtils.getExtension(filename);
			
					if(!FileUtil.ACCEPT_EXT.contains(ext.toLowerCase())) {
						res.setResult("90");
						res.setMsg("해당 파일은 업로드 하실 수 없습니다. \n 가능한 파일 :" + FileUtil.ACCEPT_EXT);
						return res;
					}
					
					fileAttch = fileAttchCreator(ufile, tmpDirResource.getPath());
					res.setResult("ok");
				} catch (Exception e) {
					res.setResult("90");
					res.setMsg("업로드 실패" + filename + " => " + e.getMessage());
					return res;
				}
			} else {
				res.setResult("90");
				res.setMsg("업로드할 파일이 없습니다.");
				return res;
			}
			
		res.setValue(fileAttch);
		return res;
	}	
	

	@RequestMapping(value = "/board/image", method = RequestMethod.POST)
	public ResponseObj boardImage(@RequestParam("file") MultipartFile file,
			HttpServletRequest request, HttpSession session) {
		
		ResponseObj res = new ResponseObj();
		FileAttch fileAttch;
	
		String filename = file.getOriginalFilename();
		if (!file.isEmpty()) {
			
			try {
				String ext = FilenameUtils.getExtension(filename);
		
				if(!FileUtil.ACCEPT_EXT.contains(ext.toLowerCase())) {
					res.setResult("90");
					res.setMsg("해당 파일은 업로드 하실 수 없습니다. \\n 가능한 파일 :" + FileUtil.ACCEPT_EXT);
					return res;
				}
				
				fileAttch = fileAttchCreator(file, boardImageDirResource.getPath());
				res.setResult("ok");
			} catch (Exception e) {
				res.setResult("90");
				res.setMsg("업로드 실패" + filename + " => " + e.getMessage());
				return res;
			}
		} else {
			res.setResult("90");
			res.setMsg("업로드할 파일이 없습니다.");
			return res;
		}
			
		res.setValue(fileAttch.getPath()+File.separator+fileAttch.getFileName());
		return res;
	}	
	
	public FileAttch fileAttchCreator(MultipartFile file, String dirPath) throws IOException {
		Calendar cal = Calendar.getInstance();
		String year = String.valueOf(cal.get(Calendar.YEAR));
		int mon = cal.get(Calendar.MONTH)+1;
		String month = "";
		if(mon < 10) {
			month = "0"+mon;
		} else {
			month = ""+mon;
		}

		int dt = cal.get(Calendar.DATE);
		String date = "";
		if(dt < 10) {
			date = "0"+dt;
		} else {
			date = ""+dt;
		}
		
		String filename = file.getOriginalFilename();
		String ext = FilenameUtils.getExtension(filename);
		long fileSize = file.getSize();
		
		byte[] bytes = file.getBytes();
		// Creating the directory to store file
		dirPath = dirPath + File.separator + year + File.separator + month + File.separator + date;
		File dir = new File(filePath + File.separator + dirPath);
		if (!dir.exists()) dir.mkdirs();
		
		
		String fileId = StringUtil.getUUIDByTimeBased().replace("-", "");
		
		System.out.println("-------------------------------------");
		System.out.println(fileId);
		System.out.println(filePath);
		System.out.println(dirPath);
		System.out.println("-------------------------------------------------");
		
		
		// Create the file on server
		File serverFile = new File(dir.getAbsolutePath() + File.separator + fileId + "." + ext);
		 
		if (serverFile.exists()) {
			serverFile.delete();
		}

		BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(serverFile));
		stream.write(bytes);
		stream.close();
		//boolean check = FileUtil.createThumbnail(tempFile, serverFile, 700);
		
		logger.info("Server File Location=" + serverFile.getAbsolutePath());
		FileAttch fileAttch = new FileAttch();
		fileAttch.setExt(ext);
		fileAttch.setFileName(fileId + "." + ext);
		fileAttch.setOrgFileName(filename);
		fileAttch.setPath(dirPath);
		fileAttch.setSize(fileSize);
		fileAttch.setNew(true);
		
		return fileAttch;
	}

}
