package kr.or.koms.board.model;

import java.util.Date;

import kr.or.koms.file.model.FileAttch;

public class Reply {

	private int id;
	private int refIdx;
	private String comment;
	private Date regDate;
	private int regIdx;
	private Date chgDate;
	private int chgIdx;

	private FileAttch fileAttch;
	
	private int isUpdateReq = 0;
	private String boardTitle = "";
	private String regEmail = ""; // 등록자이메일
	private String refEmail = ""; // 참조이메일

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getRefIdx() {
		return refIdx;
	}

	public void setRefIdx(int refIdx) {
		this.refIdx = refIdx;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public Date getRegDate() {
		return regDate;
	}

	public void setRegDate(Date regDate) {
		this.regDate = regDate;
	}

	public int getRegIdx() {
		return regIdx;
	}

	public void setRegIdx(int regIdx) {
		this.regIdx = regIdx;
	}

	public Date getChgDate() {
		return chgDate;
	}

	public void setChgDate(Date chgDate) {
		this.chgDate = chgDate;
	}

	public int getChgIdx() {
		return chgIdx;
	}

	public void setChgIdx(int chgIdx) {
		this.chgIdx = chgIdx;
	}

	public FileAttch getFileAttch() {
		return fileAttch;
	}

	public void setFileAttch(FileAttch fileAttch) {
		this.fileAttch = fileAttch;
	}

	public int getIsUpdateReq() {
		return isUpdateReq;
	}

	public void setIsUpdateReq(int isUpdateReq) {
		this.isUpdateReq = isUpdateReq;
	}

	public String getBoardTitle() {
		return boardTitle;
	}

	public void setBoardTitle(String boardTitle) {
		this.boardTitle = boardTitle;
	}

	public String getRegEmail() {
		return regEmail;
	}

	public void setRegEmail(String regEmail) {
		this.regEmail = regEmail;
	}

	public String getRefEmail() {
		return refEmail;
	}

	public void setRefEmail(String refEmail) {
		this.refEmail = refEmail;
	}

	@Override
	public String toString() {
		return "Reply [id=" + id + ", refIdx=" + refIdx + ", comment=" + comment + ", regDate=" + regDate + ", regIdx="
				+ regIdx + ", chgDate=" + chgDate + ", chgIdx=" + chgIdx + "]";
	}

}
