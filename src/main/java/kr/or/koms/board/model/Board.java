package kr.or.koms.board.model;

import java.util.Date;

import kr.or.koms.file.model.FileAttch;

public class Board {

	private int idx;
	private String groupNm;
	private String title;
	private String contents;
	private String memo;
	private String refEmail;
	private String url;
	private String status;
	private String statusEmer;
	private String regId;
	private String regName;
	private String changeId;
	private String changeName;
	private String gubun1;
	private String gubun2;
	private String gubun3;
	private String gubun4;
	private String regEmail;

	private Date completeDate;
	private Date regDate;
	private Date endReqdate;
	private Date changeDate;
	
	private String important;

	private FileAttch fileAttch;

	public int getIdx() {
		return idx;
	}

	public void setIdx(int idx) {
		this.idx = idx;
	}

	public String getGroupNm() {
		return groupNm;
	}

	public void setGroupNm(String groupNm) {
		this.groupNm = groupNm;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContents() {
		return contents;
	}

	public void setContents(String contents) {
		this.contents = contents;
	}

	public String getMemo() {
		return memo;
	}

	public void setMemo(String memo) {
		this.memo = memo;
	}

	public String getRefEmail() {
		return refEmail;
	}

	public void setRefEmail(String refEmail) {
		this.refEmail = refEmail;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getStatusEmer() {
		return statusEmer;
	}

	public void setStatusEmer(String statusEmer) {
		this.statusEmer = statusEmer;
	}

	public String getRegId() {
		return regId;
	}

	public void setRegId(String regId) {
		this.regId = regId;
	}

	public String getRegName() {
		return regName;
	}

	public void setRegName(String regName) {
		this.regName = regName;
	}

	public String getChangeId() {
		return changeId;
	}

	public void setChangeId(String changeId) {
		this.changeId = changeId;
	}

	public String getChangeName() {
		return changeName;
	}

	public void setChangeName(String changeName) {
		this.changeName = changeName;
	}

	public String getGubun1() {
		return gubun1;
	}

	public void setGubun1(String gubun1) {
		this.gubun1 = gubun1;
	}

	public String getGubun2() {
		return gubun2;
	}

	public void setGubun2(String gubun2) {
		this.gubun2 = gubun2;
	}

	public String getGubun3() {
		return gubun3;
	}

	public void setGubun3(String gubun3) {
		this.gubun3 = gubun3;
	}

	public String getGubun4() {
		return gubun4;
	}

	public void setGubun4(String gubun4) {
		this.gubun4 = gubun4;
	}

	public String getRegEmail() {
		return regEmail;
	}

	public void setRegEmail(String regEmail) {
		this.regEmail = regEmail;
	}

	public Date getCompleteDate() {
		return completeDate;
	}

	public void setCompleteDate(Date completeDate) {
		this.completeDate = completeDate;
	}

	public Date getRegDate() {
		return regDate;
	}

	public void setRegDate(Date regDate) {
		this.regDate = regDate;
	}

	public Date getEndReqdate() {
		return endReqdate;
	}

	public void setEndReqdate(Date endReqdate) {
		this.endReqdate = endReqdate;
	}

	public Date getChangeDate() {
		return changeDate;
	}

	public void setChangeDate(Date changeDate) {
		this.changeDate = changeDate;
	}

	public String getImportant() {
		return important;
	}

	public void setImportant(String important) {
		this.important = important;
	}

	public FileAttch getFileAttch() {
		return fileAttch;
	}

	public void setFileAttch(FileAttch fileAttch) {
		this.fileAttch = fileAttch;
	}

	@Override
	public String toString() {
		return "Board [idx=" + idx + ", groupNm=" + groupNm + ", title=" + title + ", contents=" + contents
				+ ", refEmail=" + refEmail + ", url=" + url + ", status=" + status + ", statusEmer=" + statusEmer
				+ ", regId=" + regId + ", regName=" + regName + ", changeId=" + changeId + ", changeName=" + changeName
				+ ", gubun1=" + gubun1 + ", gubun2=" + gubun2 + ", gubun3=" + gubun3 + ", gubun4=" + gubun4
				+ ",regEmail=" + regEmail + ", completeDate=" + completeDate + ", regDate=" + regDate + ", endReqdate="
				+ endReqdate + ", changeDate=" + changeDate + ", fileAttch=" + fileAttch + "]";
	}

}
