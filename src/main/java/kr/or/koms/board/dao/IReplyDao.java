package kr.or.koms.board.dao;

import java.util.List;
import java.util.Map;

import com.snbplus.util.TableMap;

import kr.or.koms.board.model.Reply;

public interface IReplyDao {
	// ADD
	public List<TableMap> selectByRefId(int id);
	
	public int selectReplyCnt(Map<String, Object> params);
	
	// DEFAULT
	public List<Reply> selectAll();
	
	public Reply selectById(int id);
	
	public int insert(Reply reply);

	public int update(Reply reply);
	
	public int delete(int id);

	
}
