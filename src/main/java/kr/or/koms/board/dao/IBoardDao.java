package kr.or.koms.board.dao;

import java.util.List;

import com.snbplus.util.SearchPage;
import com.snbplus.util.TableMap;

import kr.or.koms.board.model.Board;

public interface IBoardDao {

	public List<TableMap> selectPaging(SearchPage searchPage);
	
	public int selectTotalCnt(SearchPage searchPage);
	
	public int hitsUp(int id);
	
	public TableMap selectById(int id);
	
	public int insert(Board board);
	
	public int update(Board board);
	
	public int updateStatus(Board board);
	
	public int delete(int id);
}
