package kr.or.koms.board.web;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;


import javax.mail.MessagingException;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.snbplus.util.PagingModel;
import com.snbplus.util.ResponseObj;
import com.snbplus.util.SearchPage;
import com.snbplus.util.TableMap;

import kr.or.koms.board.model.Board;
import kr.or.koms.board.model.Reply;
import kr.or.koms.board.service.IBoardService;
import kr.or.koms.file.service.IFileService;
import kr.or.koms.file.web.FileDownloadController;
import kr.or.koms.member.model.Member;

@RestController
public class BoardController {
	private static Logger logger = LoggerFactory.getLogger(FileDownloadController.class);
	
	//콘분위
	private static String slackurl = "https://hooks.slack.com/services/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";
	 
	
	@Autowired
	private IBoardService boardService;
	
	@Autowired
	private IFileService fileService; 
	

	@Autowired
	private JavaMailSender mailSender;
	
		
	//게시글 list
	@RequestMapping(value= "/board/list", method = RequestMethod.GET)
	public ModelAndView getList(SearchPage srchPage, HttpServletRequest req, HttpSession session){
		ModelAndView mv = new ModelAndView();
		
		try {
			if(srchPage.getSrchValue() != null)
				srchPage.setSrchValue(URLDecoder.decode(srchPage.getSrchValue(), "UTF-8"));
		} catch (UnsupportedEncodingException e) {
			
		}

		List<TableMap> list = boardService.getBoardList(srchPage);
		int totalSize = boardService.getTotalCnt(srchPage);
		PagingModel pm = new PagingModel(srchPage.getPage(), totalSize, srchPage.getPageSize(), 10);

		mv.addObject("srchPage", srchPage);
		mv.addObject("board_list", list);
		mv.addObject("page", pm);
		mv.setViewName("/board/list");
		
		return mv;
	}
	
	//게시글 상세
	@RequestMapping(value= "/board/{id}", method = RequestMethod.GET)
	public ModelAndView readBoard(@PathVariable int id, SearchPage srchInfo, HttpServletRequest req, HttpSession session){
		ModelAndView mv = new ModelAndView();
		TableMap board = boardService.readBoard(id);
		
//		String contents = board.getString("contents");
//      contents = contents.replace("\r\n", "<br>");
//      contents = contents.replace("\u0020", "&nbsp");
		
		String memo = board.getString("memo");
		memo = memo.replace("\r\n", "<br>");
		memo = memo.replace("\u0020", "&nbsp");
        board.put("memo", memo);
		
		mv.addObject("srchInfo", srchInfo);
		mv.addObject("board", board);
		mv.setViewName("/board/view");
	
		return mv;
	}
	
	//게시글 쓰기 페이지 이동
	@RequestMapping(value= "board/new", method = RequestMethod.GET)
	public ModelAndView getWriteForm(HttpServletRequest req, HttpSession session){
		ModelAndView mv = new ModelAndView();
		mv.setViewName("/board/write");
		
		return mv;
	}
		
	
	//글쓰기
	@RequestMapping(value= "/board", method = RequestMethod.POST)
	public ResponseObj create(@RequestBody Board board, HttpServletRequest req) throws Exception{
		ResponseObj res = new ResponseObj();
		int result = boardService.create(board);
		
		String referEmail = board.getRefEmail() == null ? "" : board.getRefEmail();
		String subject = "[한국고용정보원 유지보수 요청] " + board.getTitle();

		String gubun1Nm = board.getGubun1() == null ? "X@null" : board.getGubun1();
		String gubun2Nm = board.getGubun2() == null ? "X@null" : board.getGubun2();
		String gubun3Nm = board.getGubun3() == null ? "X@null" : board.getGubun3();
		
		
		String[] gubun1= gubun1Nm.split("@");board.setGubun1(gubun1[0]);
		String[] gubun2= gubun2Nm.split("@");board.setGubun2(gubun2[0]);
		String[] gubun3= gubun3Nm.split("@");board.setGubun3(gubun3[0]);
		
		SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd");
		
		String groupNm = 	board.getGroupNm() == null ? "" : 		board.getGroupNm() ;
		                                                                                
		String title = 		board.getTitle() == null ? "" : 		board.getTitle() ;
		                                                                               
		String regName = 	board.getRegName() == null ? "" : 		board.getRegName() ;
		                                                                                 
		String refEmail =   board.getRefEmail() == null ? "" : 		board.getRefEmail() ;
		                                                                                 
		String endReqdate = board.getEndReqdate() == null ? "" : 	dt.format(board.getEndReqdate());
		                                                                                 
		String regDate = 	board.getRegDate() == null ? "" : 	dt.format(board.getRegDate());
		                                                                                 
		String url = 		board.getUrl() == null ? "" : 			board.getUrl() ;
		                                                                                 
		String contents = 	board.getContents() == null ? "" : 		board.getContents() ;
		
		String content = "<!DOCTYPE html>\r\n" + 
				"<html lang=\"ko\">\r\n" + 
				"\r\n" + 
				"<!-- 마케팅 수신동의 -->\r\n" + 
				"<table bgcolor=\"#fff\" cellpadding=\"0\" cellspacing=\"0\" width=\"700\" style=\"margin:0 auto;\">\r\n" + 
				"  <tbody>\r\n" + 
				"    <!-- 메일 상단  -->\r\n" + 
				"    <tr>\r\n" + 
				"      <td>\r\n" + 
				"        <table width=\"100%\">\r\n" + 
				"          <tr>\r\n" + 
				"            <td align=\"center\" style=\"padding:25px 0;\">\r\n" + 
				"              <img src=\"./images/logo.png\" alt=\"\" style=\"display:block;border:0;margin:0;padding:0;\">\r\n" + 
				"            </td>\r\n" + 
				"            <tr>\r\n" + 
				"              <td align=\"center\" style=\"padding:40px 0;border-top:1px solid #ddd;border-bottom:1px solid #ddd;\">\r\n" + 
				"                <p style=\"padding:0;margin:0;font-size:25px;color:#666;line-height:1.5\">\r\n" + 
				"                  <span style=\"color:#333\">"+regName+"</span>님께서<br>등록하신 접수입니다.</p>\r\n" + 
				"              </td>\r\n" + 
				"            </tr>\r\n" + 
				"          </tr>\r\n" + 
				"        </table>\r\n" + 
				"      </td>\r\n" + 
				"    </tr>\r\n" + 
				"    <!-- //메일 상단  -->\r\n" + 
				"    <!-- 메일 컨텐츠 -->\r\n" + 
				"    <tr>\r\n" + 
				"      <td style=\"padding-top:35px;font-content:'Malgun Gothic', 'Apple SD Gothic Neo', NanumGothic, dotum, gulim, sans_serif;font-size:14px;\">\r\n" + 
				"        <p style=\"padding:0;margin:0;font-size:20px;font-weight:600;color:#333;\">\r\n" + 
				"          <span><img src=\"./images/cont-title.png\" alt=\"\"></span>\r\n" + 
				"          유지보수 접수\r\n" + 
				"        </p>\r\n" + 
				"\r\n" + 
				"        <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"margin-top:20px;border-top:1px solid #888;\">\r\n" + 
				"          <tr>\r\n" + 
				"            <th bgcolor=\"#f4f4f4\" width=\"120\" align=\"left\" style=\"vertical-align:top;padding:15px 0 15px 20px;border-bottom:1px solid #e4e4e7;border-right:1px solid #e4e4e7;\">제목</th>\r\n" + 
				"            <td colspan=\"3\" style=\"padding:15px 25px;border-bottom:1px solid #e4e4e7;line-height:1.5;color:#777;\">\r\n" + 
				"              "+title+"\r\n" + 
				"            </td>\r\n" + 
				"          </tr>\r\n" + 
				"          <tr>\r\n" + 
				"            <th bgcolor=\"#f4f4f4\" width=\"120\" align=\"left\" style=\"vertical-align:top;padding:15px 0 15px 20px;border-bottom:1px solid #e4e4e7;border-right:1px solid #e4e4e7;\">작성자</th>\r\n" + 
				"            <td colspan=\"3\" style=\"padding:15px 25px;border-bottom:1px solid #e4e4e7;line-height:1.5;color:#777;\">\r\n" + 
				"              "+regName+"\r\n" + 
				"            </td>\r\n" + 
				"          </tr>\r\n" + 
				"          <tr>\r\n" + 
				"            <th bgcolor=\"#f4f4f4\" width=\"120\" align=\"left\" style=\"vertical-align:top;padding:15px 0 15px 20px;border-bottom:1px solid #e4e4e7;border-right:1px solid #e4e4e7;\">참조</th>\r\n" + 
				"            <td colspan=\"3\" style=\"padding:15px 25px;border-bottom:1px solid #e4e4e7;line-height:1.5;color:#777;\">\r\n" + 
				"              "+refEmail+"\r\n" + 
				"            </td>\r\n" + 
				"          </tr>\r\n" + 
				"          <tr>\r\n" + 
				"            <th bgcolor=\"#f4f4f4\" width=\"120\" align=\"left\" style=\"vertical-align:top;padding:15px 0 15px 20px;border-bottom:1px solid #e4e4e7;border-right:1px solid #e4e4e7;\">희망요청일</th>\r\n" + 
				"            <td style=\"padding:15px 25px;border-bottom:1px solid #e4e4e7;line-height:1.5;color:#777;\">\r\n" + 
				"              "+endReqdate+"\r\n" + 
				"            </td>\r\n" + 
				"            <th bgcolor=\"#f4f4f4\" width=\"120\" align=\"left\" style=\"vertical-align:top;padding:15px 0 15px 20px;border-bottom:1px solid #e4e4e7;border-right:1px solid #e4e4e7;\">작성일</th>\r\n" + 
				"            <td style=\"padding:15px 25px;border-bottom:1px solid #e4e4e7;line-height:1.5;color:#777;\">\r\n" + 
				"              "+regDate+"\r\n" + 
				"            </td>\r\n" + 
				"          </tr>\r\n" + 
				"          <tr>\r\n" + 
				"            <th bgcolor=\"#f4f4f4\" width=\"120\" align=\"left\" style=\"vertical-align:top;padding:15px 0 15px 20px;border-bottom:1px solid #e4e4e7;border-right:1px solid #e4e4e7;\">URL</th>\r\n" + 
				"            <td colspan=\"3\" style=\"padding:15px 25px;border-bottom:1px solid #e4e4e7;line-height:1.5;color:#777;\">\r\n" + 
				"              "+url+"\r\n" + 
				"            </td>\r\n" + 
				"          </tr>\r\n" + 
				"          <tr>\r\n" + 
				"            <th bgcolor=\"#f4f4f4\" width=\"120\" align=\"left\" style=\"vertical-align:top;padding:15px 0 15px 20px;border-bottom:1px solid #e4e4e7;border-right:1px solid #e4e4e7;\">내용</th>\r\n" + 
				"            <td colspan=\"3\" style=\"padding:15px 25px;border-bottom:1px solid #e4e4e7;line-height:1.5;color:#777;\">\r\n" + 
				contents+
				"            </td>\r\n" + 
				"          </tr>\r\n" + 
				"        </table>\r\n" + 
				"      </td>\r\n" + 
				"    </tr>\r\n" + 
				"    <!-- //메일 컨텐츠 -->\r\n" + 
				"    <!-- 메일 공통 바로가기 -->\r\n" + 
				"    <tr>\r\n" + 
				"      <td valign=\"middle\" align=\"center\" style=\"padding:30px 0;font-content:'Malgun Gothic', 'Apple SD Gothic Neo', NanumGothic, dotum, gulim, sans_serif;letter-spacing:-1px;\">\r\n" + 
				"        <a href=\"https://as.snbplus.co.kr:39443\" target=\"_blank\" style=\"display:block;width:160px;height:45px;line-height:45px;border-radius:5px;background:#20409a;font-size:14px;color:#fff;text-align:center;text-decoration:none;\">접수 사이트 바로가기</a>\r\n" + 
				"      </td>\r\n" + 
				"    </tr>\r\n" + 
				"    <!-- //메일 공통 바로가기 -->\r\n" + 
				"    <!-- 메일 공통하단 -->\r\n" + 
				"    <tr>\r\n" + 
				"      <td bgcolor=\"#282a2c\" align=\"center\" style=\"padding:20px 0;\">\r\n" + 
				"        <p style=\"margin:0;font-content:dotum, gulim, sans_serif;color:#959595;font-size:12px;line-height:1.8;\">\r\n" + 
				"          본 메일은 발신전용으로 문의사항은 <span style=\"text-decoration:underline;color:#fff;\">유지보수 접수사이트</span>를 이용해 주시기 바랍니다.<br>\r\n" + 
				"          회사  |  S&B PLUS   주소  |  서울시 금천구 가산디지털2로 14 대륭테크노타운 12차 1214호 <br>\r\n" + 
				"          Copyright ⓒ 2021 SNBPLUS. All rights reserved\r\n" + 
				"        </p>\r\n" + 
				"      </td>\r\n" + 
				"    </tr>\r\n" + 
				"    <!-- //메일 공통하단 -->\r\n" + 
				"  </tbody>\r\n" + 
				"</table>\r\n" + 
				"\r\n" + 
				"\r\n" + 
				"</html>\r\n" + 
				"";

		
		if(result > 0){
			
			 MimeMessage message = mailSender.createMimeMessage();
		     
			try {
				 
			    MimeMessageHelper messageHelper 
			                        = new MimeMessageHelper(message, true, "UTF-8");
				
			    String path2 = req.getSession().getServletContext().getRealPath("/WEB-INF/classes")+"/emailAddress.txt";
			    System.out.println(path2);
			    Path path = Paths.get(path2);
				Charset cs = StandardCharsets.UTF_8;
				List<String> list = new ArrayList();
				list = Files.readAllLines(path,cs);
				String a = String.join(",", list);
				InternetAddress[] toList = InternetAddress.parse(a);
			    
			    
				messageHelper.setFrom("snbplus10000@gmail.com");  // 보내는사람 생략하거나 하면 정상작동을 안함
				messageHelper.setTo(toList);
				messageHelper.setSubject(subject); // 메일제목은 생략이 가능하다
				messageHelper.setText(content, true);  // 메일 내용
				
				mailSender.send(message);
				//JS 정규식 : 문자열안 태그 제거
				//contents = contents.replace(/<br\/>/ig, "\n");
				//contents = contents.replace(/<(\/)?([a-zA-Z]*)(\s[a-zA-Z]*=[^>]*)?(\s)*(\/)?>/ig, "");
				slackSend(slackurl, "[PMS:신규]\r\n[제목]:"+title+"\r\n\r\n"+contents.replace("<br>","\r\n\r\n").replace("&nbsp;", " ").replace("<p>", "").replaceAll("</p>", "\r\n")+"\r\n"+url );


				
			} catch (MessagingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}			

		
			res.setResult("ok");
			res.setMsg(result+"");
		}else{
			res.setResult("10");
			res.setMsg("접수가 실패되었습니다.");
		}
		
		return res;
	}

	
	//수정 페이지
	@RequestMapping(value= "/board/edit/{id}", method = RequestMethod.GET)
	public ModelAndView getBoard(@PathVariable int id, String type, SearchPage srchInfo, HttpServletRequest req){
		ModelAndView mv = new ModelAndView();
		
		TableMap board = boardService.getBoard(id);
		
		mv.addObject("srchInfo", srchInfo);
		mv.addObject("board", board);
		mv.setViewName("/board/edit");
		
		return mv;
	}
	
	
    //게시글 수정
	@RequestMapping(value = "/board", method = RequestMethod.PUT)
    public ResponseObj update(@RequestBody Board board, HttpServletRequest req, HttpSession session) throws IOException, AddressException {
		ResponseObj res = new ResponseObj();
		String gubun1Nm = board.getGubun1() == null ? "X@null" : board.getGubun1();
		String gubun2Nm = board.getGubun2() == null ? "X@null" : board.getGubun2();
		String gubun3Nm = board.getGubun3() == null ? "X@null" : board.getGubun3();
		
		
		String[] gubun1= gubun1Nm.split("@");board.setGubun1(gubun1[0]);
		String[] gubun2= gubun2Nm.split("@");board.setGubun2(gubun2[0]);
		String[] gubun3= gubun3Nm.split("@");board.setGubun3(gubun3[0]);
		
		
		int result = boardService.update(board);
	
		
		String referEmail = board.getRefEmail() == null ? "" : board.getRefEmail();
		String regEmail =  board.getRegEmail() == null ? "" : board.getRegEmail();
		
		SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd");
	
	
		String []splitReferEmail = null; 
		String status = board.getStatus();
		InternetAddress[] toAddr = null;
		
		
		
		if(!referEmail.equals(""))
		{
			splitReferEmail = referEmail.split(";");
			
			toAddr = new InternetAddress[splitReferEmail.length];
			
			for(int sendCnt=0; sendCnt<splitReferEmail.length; sendCnt++)
			{
				System.out.println("splitReferEmail[sendCnt]" + splitReferEmail[sendCnt]);
				toAddr[sendCnt] = new InternetAddress (splitReferEmail[sendCnt]);
			}
			
		}
		if (result < 1 ){
			res.setResult("10");
			res.setMsg("수정을 실패했습니다.");
		} else {
			res.setResult("ok");
			if(status.equals("6"))
			{
				 MimeMessage message = mailSender.createMimeMessage();
			     
				 try {
					 
					 //메일 발송 내용 start					 
						String subject = "[한국고용정보원 유지보수 처리 완료] " + board.getTitle();


						
						
						String groupNm = 	board.getGroupNm() == null ? "" : 		board.getGroupNm() ;
						                                                                                
						String title = 		board.getTitle() == null ? "" : 		board.getTitle() ;
						                                                                               
						String regName = 	board.getRegName() == null ? "" : 		board.getRegName() ;
						                                                                                 
						String refEmail =   board.getRefEmail() == null ? "" : 		board.getRefEmail() ;
						                                                                                 
						String endReqdate = board.getEndReqdate() == null ? "" : 	dt.format(board.getEndReqdate());
						                                                                                 
						String regDate = 	board.getRegDate() == null ? "" : 	dt.format(board.getRegDate());
						                                                                                 
						String url = 		board.getUrl() == null ? "" : 			board.getUrl() ;
						                                                                                 
						String contents = 	board.getContents() == null ? "" : 		board.getContents() ;
						
						String content = "<!DOCTYPE html>\r\n" + 
								"<html lang=\"ko\">\r\n" + 
								"\r\n" + 
								"<!-- 마케팅 수신동의 -->\r\n" + 
								"<table bgcolor=\"#fff\" cellpadding=\"0\" cellspacing=\"0\" width=\"700\" style=\"margin:0 auto;\">\r\n" + 
								"  <tbody>\r\n" + 
								"    <!-- 메일 상단  -->\r\n" + 
								"    <tr>\r\n" + 
								"      <td>\r\n" + 
								"        <table width=\"100%\">\r\n" + 
								"          <tr>\r\n" + 
								"            <td align=\"center\" style=\"padding:25px 0;\">\r\n" + 
								"              <img src=\"./images/logo.png\" alt=\"\" style=\"display:block;border:0;margin:0;padding:0;\">\r\n" + 
								"            </td>\r\n" + 
								"            <tr>\r\n" + 
								"              <td align=\"center\" style=\"padding:40px 0;border-top:1px solid #ddd;border-bottom:1px solid #ddd;\">\r\n" + 
								"                <p style=\"padding:0;margin:0;font-size:25px;color:#666;line-height:1.5\">\r\n" + 
								"                  <span style=\"color:#333\">"+regName+"</span>님께서<br>등록하신 접수입니다.</p>\r\n" + 
								"              </td>\r\n" + 
								"            </tr>\r\n" + 
								"          </tr>\r\n" + 
								"        </table>\r\n" + 
								"      </td>\r\n" + 
								"    </tr>\r\n" + 
								"    <!-- //메일 상단  -->\r\n" + 
								"    <!-- 메일 컨텐츠 -->\r\n" + 
								"    <tr>\r\n" + 
								"      <td style=\"padding-top:35px;font-content:'Malgun Gothic', 'Apple SD Gothic Neo', NanumGothic, dotum, gulim, sans_serif;font-size:14px;\">\r\n" + 
								"        <p style=\"padding:0;margin:0;font-size:20px;font-weight:600;color:#333;\">\r\n" + 
								"          <span><img src=\"./images/cont-title.png\" alt=\"\"></span>\r\n" + 
								"          유지보수 접수\r\n" + 
								"        </p>\r\n" + 
								"\r\n" + 
								"        <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"margin-top:20px;border-top:1px solid #888;\">\r\n" + 
								"          <tr>\r\n" + 
								"            <th bgcolor=\"#f4f4f4\" width=\"120\" align=\"left\" style=\"vertical-align:top;padding:15px 0 15px 20px;border-bottom:1px solid #e4e4e7;border-right:1px solid #e4e4e7;\">구분</th>\r\n" + 
								"            <td colspan=\"3\" style=\"padding:15px 25px;border-bottom:1px solid #e4e4e7;line-height:1.5;color:#777;\">\r\n" + 
								"              작업 사이트 : <span style=\"color:#535353;\">"+gubun1[1]+"</span><br>\r\n" + 
								//"              작업 요청 : <span style=\"color:#535353;\">"+gubun2[1]+"</span><br>\r\n" + 
								//"              작업 구분 : <span style=\"color:#535353;\">"+gubun3[1]+"</span>\r\n" + 
								"            </td>\r\n" + 
								"          </tr>\r\n" + 
								"          <tr>\r\n" + 
								"            <th bgcolor=\"#f4f4f4\" width=\"120\" align=\"left\" style=\"vertical-align:top;padding:15px 0 15px 20px;border-bottom:1px solid #e4e4e7;border-right:1px solid #e4e4e7;\">제목</th>\r\n" + 
								"            <td colspan=\"3\" style=\"padding:15px 25px;border-bottom:1px solid #e4e4e7;line-height:1.5;color:#777;\">\r\n" + 
								"              "+title+"\r\n" + 
								"            </td>\r\n" + 
								"          </tr>\r\n" + 
								"          <tr>\r\n" + 
								"            <th bgcolor=\"#f4f4f4\" width=\"120\" align=\"left\" style=\"vertical-align:top;padding:15px 0 15px 20px;border-bottom:1px solid #e4e4e7;border-right:1px solid #e4e4e7;\">작성자</th>\r\n" + 
								"            <td colspan=\"3\" style=\"padding:15px 25px;border-bottom:1px solid #e4e4e7;line-height:1.5;color:#777;\">\r\n" + 
								"              "+regName+"\r\n" + 
								"            </td>\r\n" + 
								"          </tr>\r\n" + 
								"          <tr>\r\n" + 
								"            <th bgcolor=\"#f4f4f4\" width=\"120\" align=\"left\" style=\"vertical-align:top;padding:15px 0 15px 20px;border-bottom:1px solid #e4e4e7;border-right:1px solid #e4e4e7;\">참조</th>\r\n" + 
								"            <td colspan=\"3\" style=\"padding:15px 25px;border-bottom:1px solid #e4e4e7;line-height:1.5;color:#777;\">\r\n" + 
								"              "+refEmail+"\r\n" + 
								"            </td>\r\n" + 
								"          </tr>\r\n" + 
								"          <tr>\r\n" + 
								"            <th bgcolor=\"#f4f4f4\" width=\"120\" align=\"left\" style=\"vertical-align:top;padding:15px 0 15px 20px;border-bottom:1px solid #e4e4e7;border-right:1px solid #e4e4e7;\">희망요청일</th>\r\n" + 
								"            <td style=\"padding:15px 25px;border-bottom:1px solid #e4e4e7;line-height:1.5;color:#777;\">\r\n" + 
								"              "+endReqdate+"\r\n" + 
								"            </td>\r\n" + 
								"            <th bgcolor=\"#f4f4f4\" width=\"120\" align=\"left\" style=\"vertical-align:top;padding:15px 0 15px 20px;border-bottom:1px solid #e4e4e7;border-right:1px solid #e4e4e7;\">작성일</th>\r\n" + 
								"            <td style=\"padding:15px 25px;border-bottom:1px solid #e4e4e7;line-height:1.5;color:#777;\">\r\n" + 
								"              "+regDate+"\r\n" + 
								"            </td>\r\n" + 
								"          </tr>\r\n" + 
								"          <tr>\r\n" + 
								"            <th bgcolor=\"#f4f4f4\" width=\"120\" align=\"left\" style=\"vertical-align:top;padding:15px 0 15px 20px;border-bottom:1px solid #e4e4e7;border-right:1px solid #e4e4e7;\">URL</th>\r\n" + 
								"            <td colspan=\"3\" style=\"padding:15px 25px;border-bottom:1px solid #e4e4e7;line-height:1.5;color:#777;\">\r\n" + 
								"              "+url+"\r\n" + 
								"            </td>\r\n" + 
								"          </tr>\r\n" + 
								"          <tr>\r\n" + 
								"            <th bgcolor=\"#f4f4f4\" width=\"120\" align=\"left\" style=\"vertical-align:top;padding:15px 0 15px 20px;border-bottom:1px solid #e4e4e7;border-right:1px solid #e4e4e7;\">내용</th>\r\n" + 
								"            <td colspan=\"3\" style=\"padding:15px 25px;border-bottom:1px solid #e4e4e7;line-height:1.5;color:#777;\">\r\n" + 
								contents+
								"            </td>\r\n" + 
								"          </tr>\r\n" + 
								"        </table>\r\n" + 
								"      </td>\r\n" + 
								"    </tr>\r\n" + 
								"    <!-- //메일 컨텐츠 -->\r\n" + 
								"    <!-- 메일 공통 바로가기 -->\r\n" + 
								"    <tr>\r\n" + 
								"      <td valign=\"middle\" align=\"center\" style=\"padding:30px 0;font-content:'Malgun Gothic', 'Apple SD Gothic Neo', NanumGothic, dotum, gulim, sans_serif;letter-spacing:-1px;\">\r\n" + 
								"        <a href=\"https://as.snbplus.co.kr:39443\" target=\"_blank\" style=\"display:block;width:160px;height:45px;line-height:45px;border-radius:5px;background:#20409a;font-size:14px;color:#fff;text-align:center;text-decoration:none;\">접수 사이트 바로가기</a>\r\n" + 
								"      </td>\r\n" + 
								"    </tr>\r\n" + 
								"    <!-- //메일 공통 바로가기 -->\r\n" + 
								"    <!-- 메일 공통하단 -->\r\n" + 
								"    <tr>\r\n" + 
								"      <td bgcolor=\"#282a2c\" align=\"center\" style=\"padding:20px 0;\">\r\n" + 
								"        <p style=\"margin:0;font-content:dotum, gulim, sans_serif;color:#959595;font-size:12px;line-height:1.8;\">\r\n" + 
								"          본 메일은 발신전용으로 문의사항은 <span style=\"text-decoration:underline;color:#fff;\">유지보수 접수사이트</span>를 이용해 주시기 바랍니다.<br>\r\n" + 
								"          회사  |  S&B PLUS   주소  |  서울시 금천구 가산디지털2로 14 대륭테크노타운 12차 1214호 <br>\r\n" + 
								"          Copyright © 2021 SNBPLUS. All rights reserved\r\n" + 
								"        </p>\r\n" + 
								"      </td>\r\n" + 
								"    </tr>\r\n" + 
								"    <!-- //메일 공통하단 -->\r\n" + 
								"  </tbody>\r\n" + 
								"</table>\r\n" + 
								"\r\n" + 
								"\r\n" + 
								"</html>\r\n" + 
								"";
					 //메일 발송 내용 end
						
						
				     MimeMessageHelper messageHelper 
				                        = new MimeMessageHelper(message, true, "UTF-8");

				    
				    
			    		messageHelper.setFrom("snbplus10000@gmail.com");  // 보내는사람 생략하거나 하면 정상작동을 안함
						messageHelper.setTo(regEmail);
						
						if(toAddr != null){messageHelper.setCc(toAddr);}     // 받는사람 이메일(참조)
						
						messageHelper.setSubject(subject); // 메일제목은 생략이 가능하다
						messageHelper.setText(content,true);  // 메일 내용
					
						mailSender.send(message);
						slackSend(slackurl, "[PMS:완료건]\r\n"+title+"\r\n\r\n"+contents.replace("<p>", "").replaceAll("</p>", "\r\n") );

	
					
				} catch (MessagingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}else if(status.equals("4")) {
				 MimeMessage message = mailSender.createMimeMessage();
				 String content = "";
			     
			 try {
				 
				 // String boardUrl = "http://localhost:8080/board/" + board.getIdx();
				 String boardUrl = "https://as.snbplus.co.kr:39443/board/" + board.getIdx();
				 String subject = "[한국고용정보원 유지보수 수정 요청] " + board.getTitle();
				 content = "작업하신 부분에 대해서 수정 요청 사항이 발생했습니다.\r\n"+
				 				  "해당 게시글 url은 다음과 같습니다. : "+  boardUrl;
				
				

				String path2 = req.getSession().getServletContext().getRealPath("/WEB-INF/classes")+"/UPDATE_emailAddress.txt";
				System.out.println(path2);
				Path path = Paths.get(path2);
				Charset cs = StandardCharsets.UTF_8;
				List<String> list = new ArrayList();
				list = Files.readAllLines(path,cs);
				String mails = String.join(",", list);
				InternetAddress[] toList = InternetAddress.parse(mails);
				

			    MimeMessageHelper messageHelper 
                	= new MimeMessageHelper(message, true, "UTF-8");



				messageHelper.setFrom("snbplus10000@gmail.com");  // 보내는사람 생략하거나 하면 정상작동을 안함
				messageHelper.setTo(toList);
				if(toAddr != null){messageHelper.setCc(toAddr);}     // 받는사람 이메일(참조)
				
				messageHelper.setSubject(subject); // 메일제목은 생략이 가능하다
				messageHelper.setText(content,true);  // 메일 내용
				
				slackSend(slackurl, "[PMS:수정요청]\r\n[제목]:"+board.getTitle()+"\r\n\r\n"+board.getContents().replace("<br>","\r\n\r\n").replace("&nbsp;", " ").replace("<p>", "").replaceAll("</p>", "\r\n")+"\r\n "+"as.snbplus.co.kr:39443/board/" + board.getIdx());
				mailSender.send(message);
				 }catch (Exception e) {
					 e.printStackTrace();
				}
			 
			

			 
			}
			
		}
		
		return res;
	}
	

	//게시글 삭제
	@RequestMapping(value= "/board/del/{id}", method = RequestMethod.GET)
	public ResponseObj delete(@PathVariable int id, HttpServletRequest req, HttpSession session){
		ResponseObj res = new ResponseObj();
		
		Member memberInfo = (Member)req.getSession().getAttribute("user");
		int result = boardService.delete(id, memberInfo.getMember_group());
		
		if(result < 1){
			res.setResult("10");
			res.setMsg("오류가 발생했습니다.");
		}else{
			res.setResult("ok");
			res.setMsg("정상적으로 삭제되었습니다.");
		}
		
		return res;
	}
	
	
	// 댓글
	@RequestMapping(value = "/board/reply", method = RequestMethod.POST)
    public ResponseObj createReply(@RequestBody Reply reply, HttpServletRequest req, HttpSession session) throws IOException {
		ResponseObj res = new ResponseObj();
		
		Member memberInfo = (Member) session.getAttribute("user");
		
		String group = memberInfo.getMember_group();
		
		
		reply.setRegIdx(memberInfo.getIdx());
		int check = boardService.createReply(reply);

		if (check < 1) {
			res.setResult("10");
			res.setMsg("등록 실패");
		} else {
			TableMap board = boardService.getBoard(reply.getRefIdx());
			String boardUrl = "https://as.snbplus.co.kr:39443/board/" + board.getString("idx");
			String subject = "[한국고용정보원 유지보수 댓글등록]"+ reply.getBoardTitle() + "(" + boardUrl + ")";
			String content = reply.getComment();
//			content = content.replace("\r\n", "<br>");
//			content = content.replace("\u0020", "&nbsp");
			String from = "snbplus10000@gmail.com";
//			String to = "hotdari90@snbplus.co.kr,ynng3@snbplus.co.kr";
			String to = "yoojaeyeong@snbplus.co.kr";
			String[] splitReferEmail = null;
			InternetAddress[] toAddr = null;
			SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd");

			try {
				if ("1".equals(group)) {
					String referEmail = reply.getRefEmail();
					if (!referEmail.equals("")) {
						splitReferEmail = referEmail.split(";");

						toAddr = new InternetAddress[splitReferEmail.length];

						for (int sendCnt = 0; sendCnt < splitReferEmail.length; sendCnt++) {
							System.out.println("splitReferEmail[sendCnt]" + splitReferEmail[sendCnt]);
							toAddr[sendCnt] = new InternetAddress(splitReferEmail[sendCnt]);
						}
					}
					
					subject = "[한국고용정보원 유지보수 댓글등록]"+ reply.getBoardTitle() + "(" + boardUrl + ")";
					to = reply.getRegEmail();

					MimeMessage message = mailSender.createMimeMessage();

					MimeMessageHelper messageHelper = new MimeMessageHelper(message, true, "UTF-8");

					messageHelper.setFrom(from); // 보내는사람 생략하거나 하면 정상작동을 안함
					messageHelper.setTo(to); // 받는사람 이메일

					messageHelper.setSubject(subject); // 메일제목은 생략이 가능하다
					if(toAddr != null){messageHelper.setCc(toAddr);}     // 받는사람 이메일
					messageHelper.setText(content); // 메일 내용

					mailSender.send(message);
					slackSend(slackurl, "[PMS:댓글등록]\r\n[제목]:"+reply.getBoardTitle()+"\r\n\r\n"+reply.getComment().replace("<br>","\r\n\r\n").replace("&nbsp;", " ").replace("<p>", "").replaceAll("</p>", "\r\n")+"\r\n "+"as.snbplus.co.kr:39443/board/" + reply.getRefIdx());
				}


			} catch (MessagingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			res.setResult("ok");
		}

		return res;
	}
	
	
	
	
	//엑셀 다운로드
	@RequestMapping(value= "/board/excel", method = RequestMethod.GET)
	public void downloadExcel(SearchPage srchPage, HttpServletResponse res, HttpServletRequest req, HttpSession session) throws Exception {
		
		boardService.getExcel(srchPage, res, req);
	}
	
	// slack message send
	private void slackSend(String url, String content) throws IOException {
		System.out.println("slackSend:호출됨");
		CloseableHttpClient client = HttpClients.createDefault(); 
		HttpPost httpPost = new HttpPost(url); 

		String json = "{ \"text\": \""+ content.toString() + "\" }"; 
	
		StringEntity entity = new StringEntity(json, "UTF-8"); 
		httpPost.setEntity(entity); 
		httpPost.setHeader("Accept-Encoding", "UTF-8");
		httpPost.setHeader("Accept", "application/json"); 
		httpPost.setHeader("Content-type", "application/json"); 
		CloseableHttpResponse response = client.execute(httpPost); 
		logger.error("getStatusCode : " + response.getStatusLine().getStatusCode() ); 
		//assertThat( response.getStatusLine().getStatusCode(), equals(200) ); 
		client.close();
	}
	



}

