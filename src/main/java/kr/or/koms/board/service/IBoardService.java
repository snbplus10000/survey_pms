package kr.or.koms.board.service;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.snbplus.util.SearchPage;
import com.snbplus.util.TableMap;

import kr.or.koms.board.model.Board;
import kr.or.koms.board.model.Reply;

public interface IBoardService {
	
	public List<TableMap> getBoardList(SearchPage searchPage);
	
	public TableMap readBoard(int id);
	
	public TableMap getBoard(int id);
	
	public int getTotalCnt(SearchPage searchPage);

	public int create(Board board) throws IOException;
	
	public int update(Board board) throws IOException;
	
	public int delete(int id, String userId);
		
	public int createReply(Reply reply) throws IOException;
	
	public void getExcel(SearchPage searchPage, HttpServletResponse response, HttpServletRequest request) throws Exception;
}
