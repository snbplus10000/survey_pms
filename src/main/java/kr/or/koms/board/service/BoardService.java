package kr.or.koms.board.service;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.stereotype.Service;

import com.snbplus.util.ExcelWriters;
import com.snbplus.util.FileUtil;
import com.snbplus.util.SearchPage;
import com.snbplus.util.TableMap;

import kr.or.koms.file.dao.IFileAttchDao;
import kr.or.koms.file.model.FileAttch;
import kr.or.koms.board.dao.IBoardDao;
import kr.or.koms.board.dao.IReplyDao;
import kr.or.koms.board.model.Board;
import kr.or.koms.board.model.Reply;

@Service
public class BoardService implements IBoardService {

	@Value("${Globals.filePath}") private String filePath;
	@Autowired private FileSystemResource boardDirResource;
	
	@Autowired private IBoardDao boardDao;
	@Autowired private IFileAttchDao fileAttchDao;
	@Autowired private IReplyDao replyDao;
	
	public List<TableMap> getBoardList(SearchPage searchPage) {
		return boardDao.selectPaging(searchPage);
	}
	
	public int getTotalCnt(SearchPage searchPage) {
		return boardDao.selectTotalCnt(searchPage);
	}
	
	
	public TableMap readBoard(int id) {
				
		TableMap result = boardDao.selectById(id);
		
		if(result != null) {
			FileAttch param = new FileAttch();
			param.setRefId(id);
			param.setRefTable("BOARD");
			FileAttch selectedFile = fileAttchDao.selectByRef(param);
			List<TableMap> reply = replyDao.selectByRefId(id);
			
			for(int i=0; i < reply.size(); i++){
				  String comment = reply.get(i).getString("comment");
				  comment = comment.replace("\r\n", "<br>");
				  comment = comment.replace("\u0020", "&nbsp");
				  
				  reply.get(i).put("comment", comment);
			}
			
			result.put("fileAttch", selectedFile);
			result.put("reply", reply);
		}
		return result;
	}
	

	
	public TableMap getBoard(int id) {
		
		TableMap result = boardDao.selectById(id);

		if(result != null) {
			//HashMap<String, Object> param = new HashMap<String, Object>();
			FileAttch param = new FileAttch();
			param.setRefTable("BOARD");
			param.setRefId(id);
			FileAttch selectedFile = fileAttchDao.selectByRef(param);
			result.put("fileAttch", selectedFile);
		}
	
		return result;
	}
	
	
	
	public int create(Board board) throws IOException{
		FileAttch fa = board.getFileAttch();				
		int result = boardDao.insert(board);
		
		Calendar cal = Calendar.getInstance();
		String year = String.valueOf(cal.get(Calendar.YEAR));
		int mon = cal.get(Calendar.MONTH)+1;
		String month = "";
		if(mon < 10) {
			month = "0"+mon;
		} else {
			month = ""+mon;
		}

		int dt = cal.get(Calendar.DATE);
		String date = "";
		if(dt < 10) {
			date = "0"+dt;
		} else {
			date = ""+dt;
		}
		
		if(fa != null){
			
			File orgFile = new File(filePath+fa.getPath()+File.separator+fa.getFileName());
//			System.out.println(orgFile.getAbsolutePath());
			String dirPath = boardDirResource.getPath();
//			System.out.println(boardDirResource.getPath());
			dirPath = dirPath + File.separator + year + File.separator + month + File.separator + date;
//			System.out.println(dirPath);
			File moveFile = new File(filePath+dirPath+File.separator+fa.getFileName());
			boolean flag = FileUtil.moveToFile(orgFile, moveFile);
			FileUtil.deleteDir(filePath+fa.getPath());
			fa.setPath(dirPath);
			fa.setRefId(board.getIdx());
			
			fileAttchDao.insert(fa);
		}
		
		if(result > 0 ){
			result = board.getIdx();
		}
		
		return result;
	}
	
	
	public int update(Board board) throws IOException {
		
		FileAttch fa = board.getFileAttch();
		int result = boardDao.update(board);

		Calendar cal = Calendar.getInstance();
		String year = String.valueOf(cal.get(Calendar.YEAR));
		int mon = cal.get(Calendar.MONTH)+1;
		String month = "";
		if(mon < 10) {
			month = "0"+mon;
		} else {
			month = ""+mon;
		}

		int dt = cal.get(Calendar.DATE);
		String date = "";
		if(dt < 10) {
			date = "0"+dt;
		} else {
			date = ""+dt;
		}
		
		if(fa != null) {
			  if(fa.isNew()) {
				  
					fa.setRefId(board.getIdx());
					fileAttchDao.deleteRef(fa);
					File orgFile = new File(filePath+fa.getPath()+File.separator+fa.getFileName());
					String dirPath = boardDirResource.getPath();
					dirPath = dirPath + File.separator + year + File.separator + month + File.separator + date;
					File moveFile = new File(filePath+dirPath+File.separator+fa.getFileName());
					boolean flag = FileUtil.moveToFile(orgFile, moveFile);
//					FileUtil.deleteDir(dirPath+fa.getPath()); 
					fa.setPath(dirPath);
					
					//fileAttchDao.deleteRefId(board.getId());
					fileAttchDao.insert(fa);
				}
		}
		return result;
	}
	
	
	public int delete(int id, String userId) {
		TableMap check = boardDao.selectById(id);
		int result = 0;
		
		if(check.getString("regId").equals(userId) || userId.equals("1")){
			result = boardDao.delete(id);
		}

		return result;
	}
	
	
	public int createReply(Reply reply) throws IOException{

		if(1 == reply.getIsUpdateReq()) {
			Board board = new Board();
			
			board.setIdx(reply.getRefIdx());
			board.setStatus("4");
			boardDao.updateStatus(board);
		}
		
		FileAttch fa = reply.getFileAttch();				
		int result = replyDao.insert(reply);
		
		Calendar cal = Calendar.getInstance();
		String year = String.valueOf(cal.get(Calendar.YEAR));
		int mon = cal.get(Calendar.MONTH)+1;
		String month = "";
		if(mon < 10) {
			month = "0"+mon;
		} else {
			month = ""+mon;
		}

		int dt = cal.get(Calendar.DATE);
		String date = "";
		if(dt < 10) {
			date = "0"+dt;
		} else {
			date = ""+dt;
		}
		
		if(fa != null){
			
			File orgFile = new File(filePath+fa.getPath()+File.separator+fa.getFileName());
//			System.out.println(orgFile.getAbsolutePath());
			String dirPath = boardDirResource.getPath();
//			System.out.println(boardDirResource.getPath());
			dirPath = dirPath + File.separator + year + File.separator + month + File.separator + date;
//			System.out.println(dirPath);
			File moveFile = new File(filePath+dirPath+File.separator+fa.getFileName());
			boolean flag = FileUtil.moveToFile(orgFile, moveFile);
			FileUtil.deleteDir(filePath+fa.getPath());
			fa.setPath(dirPath);
			fa.setRefId(reply.getId());
			
			fileAttchDao.insert(fa);
		}
		
		if(result > 0 ){
			result = reply.getId();
		}
		return result;
	}
	
	public void getExcel(SearchPage searchPage, HttpServletResponse response, HttpServletRequest request) throws Exception{
		searchPage.setPageSize(0);
		List<TableMap> dataList = boardDao.selectPaging(searchPage);
		
		String uploadPath = filePath + boardDirResource.getPath() + "/Excel/"; 
		java.io.File dir = new java.io.File(uploadPath);
		dir.mkdirs();

		ExcelWriters excel = new ExcelWriters(uploadPath);
		excel.createRow(0);
		
		int idx = 0;
		
		excel.createCell(idx++);
		excel.setCellString("글번호");
		excel.setTitleCell();
		
		excel.createCell(idx++);
		excel.setCellString("분류");
		excel.setTitleCell();
		
		excel.createCell(idx++);
		excel.setCellString("팀이름");
		excel.setTitleCell();
		
		excel.createCell(idx++);
		excel.setCellString("제목");
		excel.setTitleCell();
		
		excel.createCell(idx++);
		excel.setCellString("등록자");
		excel.setTitleCell();
		
		excel.createCell(idx++);
		excel.setCellString("등록일");
		excel.setTitleCell();
		
		excel.createCell(idx++);
		excel.setCellString("완료일");
		excel.setTitleCell();
		
		excel.createCell(idx++);
		excel.setCellString("답변여부");
		excel.setTitleCell();
		
		excel.createCell(idx++);
		excel.setCellString("작업분류");
		excel.setTitleCell();
		
		if(dataList != null && dataList.size() > 0){
			for(int i = 0; i < dataList.size(); i++){
				excel.createRow((i + 1));
				
				idx = 0;
				
				excel.createCell(idx++);
				excel.setBorderCell();
				excel.setCellNumeric(dataList.get(i).getInt("idx"));
				excel.setColumnWidth(idx-1);
				
				excel.createCell(idx++);
				excel.setBorderCell();
				excel.setCellString(dataList.get(i).getString("gubun2Nm"));
				excel.setColumnWidth(idx-1);
				
				excel.createCell(idx++);
				excel.setBorderCell();
				excel.setCellString(dataList.get(i).getString("groupNm"));
				excel.setColumnWidth(idx-1);
				
				excel.createCell(idx++);
				excel.setBorderCell();
				excel.setCellString(dataList.get(i).getString("title"));
				excel.setColumnWidth(idx-1);
				
				excel.createCell(idx++);
				excel.setBorderCell();
				excel.setCellString(dataList.get(i).getString("regName"));
				excel.setColumnWidth(idx-1);
				
				excel.createCell(idx++);
				excel.setBorderCell();
				excel.setCellDate(dataList.get(i).getString("regDate"));
				excel.setColumnWidth(idx-1);
				
				excel.createCell(idx++);
				excel.setBorderCell();
				excel.setCellString(dataList.get(i).getString("completeDate"));
				excel.setColumnWidth(idx-1);
				
				excel.createCell(idx++);
				excel.setBorderCell();
				excel.setCellString(dataList.get(i).getString("statusNm"));
				excel.setColumnWidth(idx-1);
				
				excel.createCell(idx++);
				excel.setBorderCell();
				excel.setCellString(dataList.get(i).getString("gubun4Nm"));
				excel.setColumnWidth(idx-1);
			}
		}
		
	 	 String fileName = "SURVEY_PMS_LIST.xlsx";
	 	 excel.completeExcel();
		 excel.download(fileName, response, request);
	}
	
	
}