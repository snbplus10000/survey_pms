package kr.or.koms.member.web;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import com.snbplus.util.PagingModel;
import com.snbplus.util.SearchPage;
import com.snbplus.util.TableMap;

import kr.or.koms.board.service.IBoardService;
import kr.or.koms.member.model.Member;
import kr.or.koms.board.model.Board;
import kr.or.koms.member.service.IMemberService;

@RestController
public class LoginController {

	@Autowired
	private IMemberService adminService;
	
	@Autowired
	private IBoardService boardService;
	
	@RequestMapping(value = "/", method = RequestMethod.GET)
    public ModelAndView loginPage(HttpServletRequest req, HttpSession session) {
		ModelAndView mv = new ModelAndView();
		
		mv.setViewName("index");
		return mv;
    }
	
	@RequestMapping(value = "/login", method = RequestMethod.POST)
    public ModelAndView login(Member member, HttpServletRequest req, HttpSession session) throws IOException {
		ModelAndView mv = new ModelAndView();
		Member result = adminService.loginCheck(member);
		
		if(result != null && !result.getId().equals("false")){
			result.setPw(null);
			session.setAttribute("user", result);
			
			return (ModelAndView)new ModelAndView("redirect:/board/list");
		}else{
			mv.addObject("msg", "no");
			mv.setViewName("/index");
			return mv;
		}
				
	}
	
	
	@RequestMapping(value = "/pwInquiry", method = {RequestMethod.GET, RequestMethod.POST})
    public ModelAndView pwInquiry(HttpServletRequest req, HttpSession session) throws IOException {
			
		ModelAndView mv = new ModelAndView();
		Member loginUser = (Member) session.getAttribute("user");
		
		if(loginUser != null)
		{
			mv.addObject("userInfo", loginUser);
			mv.setViewName("/login/pwInquiry");
			return mv;
		}
		else
		{
			mv.setViewName("/index");
			return mv;
		}
	}
	
	@RequestMapping(value = "/pwChange", method = {RequestMethod.GET, RequestMethod.POST})
    public ModelAndView pwChange(@RequestParam HashMap<String, String> param,HttpServletRequest req, HttpSession session) throws IOException {
			
		ModelAndView mv = new ModelAndView();
		Member loginUser = (Member) session.getAttribute("user");
		
		if(loginUser != null)
		{
			param.put("id", loginUser.getId());
			String rtn_msg = adminService.updateUserInfo(param);
			
			if(rtn_msg.equals("MSG_SUCCESE"))
				mv.setViewName("/index");
			else
			{
				mv.addObject("userInfo", loginUser);
				mv.addObject("msg", rtn_msg);
				mv.setViewName("/login/pwInquiry");
			}
		}
		else{
			mv.setViewName("/index");
			
		}
		
		return mv;
	}
		
}



