package kr.or.koms.member.model;

public class Member {
	
	private int idx;
	private String id;
	private String pw;
	private String name;	
	private String tel	;
	private String email;
	private String member_group;
	
	
	public int getIdx() {
		return idx;
	}
	public void setIdx(int idx) {
		this.idx = idx;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getPw() {
		return pw;
	}
	public void setPw(String pw) {
		this.pw = pw;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getTel() {
		return tel;
	}
	public void setTel(String tel) {
		this.tel = tel;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getMember_group() {
		return member_group;
	}
	public void setMember_group(String member_group) {
		this.member_group = member_group;
	}
	
	
	@Override
	public String toString() {
		return "Member [idx=" + idx + ", id=" + id + ", pw=" + pw + ", name=" + name + ", tel=" + tel + ", email="
				+ email + ", member_group=" + member_group + "]";
	}

}
