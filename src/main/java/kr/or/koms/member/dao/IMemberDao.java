package kr.or.koms.member.dao;


import java.util.HashMap;

import kr.or.koms.member.model.Member;

public interface IMemberDao {
		
	public Member selectByUser(String user);
	
	public int insert(Member admin);
	
	public void updateUserInfo(HashMap<String, String> param);
	
}
