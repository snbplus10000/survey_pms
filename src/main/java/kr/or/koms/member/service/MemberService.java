package kr.or.koms.member.service;

import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.snbplus.util.Sha256;

import kr.or.koms.member.dao.IMemberDao;
import kr.or.koms.member.model.Member;

@Service
public class MemberService implements IMemberService {
	
	@Autowired
	private IMemberDao adminDao;
	
	public Member loginCheck(Member member) {
		
		Member result = adminDao.selectByUser(member.getId());
		String shaPwd = Sha256.encrypt(member.getPw());
		//System.out.println("shaPwd " + shaPwd);
		
		if(result != null){			
			if(!shaPwd.equals(result.getPw())){
				result.setId("false");
			}
		}
		return result;
	}
		
	public String updateUserInfo(HashMap<String, String> param) {
		
		String userId = param.get("userId");
		String userPassWd = Sha256.encrypt(param.get("userPw"));
		String userNewPassWd = Sha256.encrypt(param.get("userNewPw"));
		String rtnMsg = "MSG_NOT_DATA";
		
		Member member = adminDao.selectByUser(userId);
		
		if(member != null)
		{			
			if(!userPassWd.equals(member.getPw()))
				rtnMsg = "MSG_NOT_DATA";
			else
			{
				try 
				{
					param.put("newPw", userNewPassWd);
					
					adminDao.updateUserInfo(param);
					rtnMsg = "MSG_SUCCESE";
				} 
				catch (Exception e)
				{
					rtnMsg = "MSG_NOT_UPDATE";
				}
			}
		}
		
		return rtnMsg;		
	}
	
//	public void create(Admin admin){
//		String pwd = Sha256.encrypt(admin.getPwd());
//		admin.setPwd(pwd);
//		adminDao.insert(admin);
//	}
}
