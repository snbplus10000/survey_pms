package kr.or.koms.member.service;

import java.util.HashMap;

import kr.or.koms.member.model.Member;

public interface IMemberService {
	
	public Member loginCheck(Member member);
	
	public String updateUserInfo(HashMap<String, String> param);
	
//	public void create(Admin admin);
}
