<%@ page language="java" contentType="text/html; charset=UTF-8"
	isELIgnored="false" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!-- 개별 CSS -->

	
<div id="footer">
    <div class="inner">
      <ul class="ft_info">
        <li>회사  |  S&B PLUS</li>
        <li>담당자  |  안현섭</li>
        <li>담당자 이메일  | ahs2124@snbplus.co.kr</li>
        <li>담당자번호  |  02-6952-1463</li>
      </ul>
      <ul class="ft_info">
        <li>주소  |  서울시 금천구 가산디지털2로 14 대륭테크노타운 12차 1214호</li>
        <li>팩스  |  070-8280-1470</li>
      </ul>
      <p class="ft_copy">Copyright © 2021 SNBPLUS. All rights reserved</p>
    </div>
</div>