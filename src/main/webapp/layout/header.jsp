<%@ page language="java" contentType="text/html; charset=UTF-8"
	isELIgnored="false" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

  <div id="header">
    <div class="inner">
      <div class="fl logo"><a href="/board/list"><img src="/public/img/logo.gif" style="height:50px" alt="로고"></a></div>
      <div class="rig">
        <ul class="site">
          <li>
            <span class="tit">관리자</span>
			<ul>
				<li>
					<a href="https://survey.keis.or.kr/" class='before'>홈페이지</a> 
				</li>
			</ul>
          </li>

          <li>
            <span class="tit">사이트</span>
           	<ul>
				<li>
				<a href="https://survey.keis.or.kr/" class='before'>홈페이지</a> 
				</li>
			</ul>
          </li>
        </ul>
      </div>
    </div>
  </div> 