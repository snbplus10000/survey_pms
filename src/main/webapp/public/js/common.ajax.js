var ajax = {
		get: function(url, success) {
			$('html').css('cursor', 'wait');
			$.ajax({
				type : "GET",
				url : url,
				datatype : "JSON",
				contentType : "application/json",
				success : success,
				error : function(xhr, status, error) {
					if(xhr.status == 405) {
						console.log("권한이 없습니다. 로그인하십시오.");
						location.href="/";
					} else {
						console.log(error);
					}
				},
				complete : function() {
					$('html').css('cursor', 'auto');
				}
			});
		},
		post: function(url, data, success) {
			var strJson = JSON.stringify(data);
			$.ajax({
				type : "POST",
				url : url,
				data : strJson,
				datatype : "JSON",
				contentType : "application/json",
				success : success,
				error : function(xhr, status, error) {
					if(xhr.status == 405) {
						console.log("권한이 없습니다. 로그인하십시오.");
						location.href="/";
					} else {
						console.log(error);
					}
				},
				complete : function() {
					$('html').css('cursor', 'auto');
				}
			});
		},
		put: function(url, data, success) {
			var strJson = JSON.stringify(data);
			$('html').css('cursor', 'wait');
			$.ajax({
				type : "PUT",
				url : url,
				data : strJson,
				datatype : "JSON",
				contentType : "application/json",
				success : success,
				error : function(xhr, status, error) {
					if(xhr.status == 405) {
						console.log("권한이 없습니다. 로그인하십시오.");
						location.href="/";
					} else {
						console.log(error);
					}
				},
				complete : function() {
					$('html').css('cursor', 'auto');
				}
			});
		},
		delete: function(url, success) {
			$('html').css('cursor', 'wait');
			$.ajax({
				type : "DELETE",
				url : url,
				datatype : "JSON",
				contentType : "application/json",
				success : success,
				error : function(xhr, status, error) {
					if(xhr.status == 405) {
						console.log("권한이 없습니다. 로그인하십시오.");
						location.href="/";
					} else {
						console.log(error);
					}
				},
				complete : function() {
					$('html').css('cursor', 'auto');
				}
			});
		},
		file: function(url, file, success) {
			$('html').css('cursor', 'wait');
			$.ajax({
				type : "POST",
				url : url,
		    	processData: false,
				contentType: false,
				data: file,
				type: 'POST',
				success : success,
				error : function(xhr, status, error) {
					console.log(status);
				},
				complete : function() {
					$('html').css('cursor', 'auto');
				}
			});
		}
}

