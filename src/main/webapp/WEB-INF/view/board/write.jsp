<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%> 
<%@ page isELIgnored="false" %>
<!DOCTYPE html>
<html lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
<title>한국고용정보원</title>
	
	<!-- 공통 CSS -->
	<jsp:include page="/layout/core-css.jsp"/>
	<link href="/plugin/summernote/summernote-lite.css" rel="stylesheet" type="text/css">
	
	<!-- 공통 JS -->
	<jsp:include page="/layout/core-js.jsp"/>
	<script src="/plugin/summernote/summernote-lite.js"></script>
	<script src="/plugin/summernote/lang/summernote-ko-KR.js"></script>
	
	
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
<body>
<div id="wiset_wrap">
  <div id="header">
    <div class="inner">
      <div class="fl logo"><a href="/board/list"><img src="/public/img/logo.gif" style="height:40px" alt="로고"></a></div>
      <div class="rig">
        <ul class="site">
          <li>
            <span class="tit">관리자</span>
			<ul>
				<li>
					<a href="https://gfwri.kr/" class='before'>홈페이지</a> 
				</li>
			</ul>
          </li>

          <li>
            <span class="tit">사이트</span>
           	<ul>
				<li>
				<a href="https://gfwri.kr/" class='before'>홈페이지</a> 
				</li>
			</ul>
          </li>
        </ul>
      </div>
    </div>
  </div>
  
  <!-- Contents -->
  <div id="content">
    <div class="inner">
      <!-- 상단 -->
      <div class="list_top clearfix">
        <p class="cont_title fl_lef">유지보수 접수 글 작성</p>
      </div>
      <!-- /.list_top -->
      <!-- 기본 게시판 상세보기 -->
      <form name="write_form">
      	<input type="hidden" name="regId" value="${user.id}"/>
      	<input type="hidden" name="regEmail" value="${user.email}"/>
      <table class="list_detail list_write">
        <colgroup>
          <col width="12%">
          <col width="*">
        </colgroup>
        <tbody>
          <tr>
            <th class="point">구분 <span>*</span></th>
            <td>
              <select class="board_select" name="gubun1">
                <option value="">작업 사이트 선택</option>
				<option value="F01">홈페이지</option>
				<option value="F02">관리자</option>
				<option value="F99">기타</option>
              </select>
              <select class="board_select" name="gubun2">
                <option value="">작업 요청 선택</option>
                <option value="S01">추가요청</option>
                <option value="S02">수정요청</option>
                <option value="S03">삭제요청</option>
			   	<option value="S99">기타</option>
              </select>
              <select class="board_select" name="gubun3">
		    				<option value="">작업분류</option>
							<option value="T01">개발</option>
							<option value="T02">디자인</option>
							<option value="T03">퍼블리싱</option>
							<option value="T12">개발/디자인</option>
							<option value="T23">디자인/퍼블리싱</option>
							<option value="T13">개발/퍼블리싱</option>
							<option value="T77">개발/디자인/퍼블리싱</option>
							<option value="T99">기타</option>
              </select>
            </td>
          </tr>
          <tr>
            <th class="point">
							제목 <span>*</span>
							<label for="important" title="긴급일 경우에만 체크해주세요" style="margin-left: 7px;">
							<input type="checkbox" id="important" name="important" value="Y"><span>긴급!</span>
							</label>
            </th>
            <td><input type="text" name="title"></td>
          </tr>
          <tr>
            <th class="point">작성자 <span>*</span></th>
            <td><input type="text" class="gy" name="regName" value="${user.name}" readonly="readonly"></td>
          </tr>
          <tr>
            <th>참조</th>
            <td>
              <input type="text" name="refEmail" placeholder="참조하실 분의 메일을  ;로 구분하여 적어주세요. ex: info@wiset.or.kr;master@wiset.or.kr">
            </td>
          </tr>
          <tr>
            <th class="point">희망요청일 <span>*</span></th>
            <td class="w_date">
              <input type="text" class="date" name="endReqdate" id="endReqdate">
              <span class="fs12">희망요청일을 적어주시면 스케줄 확인 후 진행하겠습니다.</span>
            </td>
          </tr>
          <tr>
            <th>URL</th>
            <td>
              <input type="text" name="url">
            </td>
          </tr>
          <tr>
            <th class="point">내용 <span>*</span></th>
            <td class="cont">
            	<div id="contents"></div>
            </td>
          </tr>
          <tr>
            <th>첨부파일</th>
            <td class="file_input">
              <div class="fl_lef">
	              <input class="upload_name" id="itemUploadInput" disabled="disabled">
	              <label for="ufile" class="btn btn_file">파일찾기</label>
	              <input type="file" class="upload_hidden" name="ufile" id="ufile">
              </div>
              <div class="txt fs12 fl_lef">
                	파일 1개 이상일 경우 zip 파일로 올려주세요.<br>업로드 가능 파일 : <span>pdf,jpeg,jpg,png,gif,bmp,tiff,txt,doc,hwp,ppt,pptx,xlsx,xls,zip,7z,rar,alz</span>
              </div>
            </td>
          </tr>

        </tbody>
      </table>
      <!-- //기본 게시판 상세보기 -->
	 </form>

      <!-- 게시판 버튼 -->
      <div class="btn_wrap al_cen">
        <a href="javascript:void(0);" class="btn" id="submit-btn">접수</a>
        <a href="/board/list" class="btn btn1">취소</a>
      </div>
      <!-- //게시판 버튼 -->
    </div>
  </div>
  <!-- /#content -->
  <footer>
     <jsp:include page="/layout/footer.jsp"></jsp:include>  
  </footer>
</div>
	
</body>
<script type="text/javascript">
var submit_ck = true;

 $(function(){

		 
	$('#contents').summernote({
	  height: 300,                 // set editor height
	  width: '100%',
	  minHeight: null,             // set minimum height of editor
	  maxHeight: null,             // set maximum height of editor
	  focus: false,                  // set focus to editable area after initializing summernote
	  lang: 'ko-KR',
	  callbacks: {
		  onImageUpload: function(files, editor, welEditable) {
			var formData = new FormData();
			formData.append("file", files[0]);
			ajax.file('/upload/board/image', formData, function(data) { 
				if(data.result != 'ok') {
					alert(data.msg);
					return false;
				}
			    var node = document.createElement("IMG");
			    node.setAttribute("src", '/ui'+data.value);
				$('#contents').summernote('insertNode', node);
				console.log(data)
			})
		  }
	  }
	});
		
	 $('#endReqdate').datepicker({
			dateFormat: 'yy-mm-dd'
		});
	 
		var file;
		
		$('input[name=ufile]').on('change', function(){
			var formData = new FormData();
			$($("#ufile")[0].files).each(function(index, file) {
				formData.append("ufile", file);
			});
			ajax.file('/upload/file', formData, function(data) { 
				if(data.result != 'ok') {
					alert(data.msg);
					return false;
				}
					file = data.value;
					$('#itemUploadInput').val(file.orgFileName);
					$.extend(file, {"sort": 0});
			})
		});
	
		$('#submit-btn').on('click', function(){
			var json = $('form[name=write_form]').serializeObject();
			if(!$('#contents').summernote('isEmpty'))
				json.contents = $('#contents').summernote('code');
			else 
				json.contents = '';
			if(!empty(file))
				file.refTable = 'BOARD';
			json.fileAttch = file;
			//alert(josn.endReqdate);

			 if(empty($.trim(json.title))) {
				alert('제목을 입력해주세요.');
				focus('title');
				return;
			}else if(empty($.trim(json.endReqdate))) {
				alert('희망요청일을 입력해주세요.');
				focus('endReqdate');
				return;
			}else if(empty($.trim(json.contents))) {
				alert('내용을 입력해주세요.');
				focus('contents');
				return;
			}else if(empty($.trim(json.gubun1))) {
				alert('작업사이트를 선택해주세요.');
				focus('gubun1');
				return;
			}else if(empty($.trim(json.gubun2))) {
				alert('작업요청을 선택해주세요.');
				focus('gubun2');
				return;
			}else if(empty($.trim(json.gubun3))) {
				alert('작업구분을 선택해주세요.');
				focus('gubun3');
				return;
			}
			if(submit_ck){
				submit_ck=false;
			ajax.post('/board', json, function(data){
				if(data.result != 'ok') {
					alert(data.msg);
					return false;
				}else{
					alert('접수되었습니다.');
					location.href='/board/'+data.msg;
				}
			});
			}else{
				alert("처리중 입니다.");
			}

		});
	 })
</script>
</html>