<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%> 
<%@ page isELIgnored="false" %>
<!DOCTYPE html>
<html lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
<title>한국고용정보원</title>
	
	<!-- 공통 CSS -->
	<jsp:include page="/layout/core-css.jsp"/>
	<link href="/plugin/summernote/summernote-lite.css" rel="stylesheet" type="text/css">
	
	<!-- 공통 JS -->
	<jsp:include page="/layout/core-js.jsp"/>
	<script src="/plugin/summernote/summernote-lite.js"></script>
	<script src="/plugin/summernote/lang/summernote-ko-KR.js"></script>
	
	
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<div id="wiset_wrap">
	<jsp:include page="/layout/header.jsp"></jsp:include> 
  
  <!-- Contents -->
  <div id="content">
    <div class="inner">
      <!-- 상단 -->
      <div class="list_top clearfix">
        <p class="cont_title fl_lef">유지보수 접수 글 수정</p>
      </div>
      <!-- /.list_top -->
      <!-- 기본 게시판 상세보기 -->
      <form name="edit_form">
     	<input type="hidden" name="idx" value="${board.idx }">
   		<input type="hidden" name="refEmail" value="${board.refEmail}">
   		<input type="hidden" name="regEmail" value="${board.regEmail}">   		
   		
   		<input type="hidden" name="gubun1" value="">
   		<input type="hidden" name="gubun2" value="">
   		<input type="hidden" name="gubun3" value="">
   		
   		
      <table class="list_detail list_write">
        <colgroup>
          <col width="12%">
          <col width="*">
        </colgroup>
        <tbody>
          <tr>
            <th class="point">구분 <span>*</span></th>
            <td>
              <select class="board_select" name="gubun1sel" id="gubun1sel" >
		    				<option value="F01" ${board.gubun1 eq 'F01'? 'selected="selected"': ''}>홈페이지</option>
		    				<option value="F02" ${board.gubun1 eq 'F02'? 'selected="selected"': ''}>관리자</option>
							<option value="F99" ${board.gubun1 eq 'F99'? 'selected="selected"': ''}>기타</option>
		    			</select>
		    			<select class="board_select" name="gubun2sel" id="gubun2sel">
		    				<option value="S01" ${board.gubun2 eq 'S01'? 'selected="selected"': ''}>추가요청</option>
		    				<option value="S02" ${board.gubun2 eq 'S02'? 'selected="selected"': ''}>수정요청</option>
		    				<option value="S03" ${board.gubun2 eq 'S03'? 'selected="selected"': ''}>삭제요청</option>
		    				<option value="S99" ${board.gubun2 eq 'S99'? 'selected="selected"': ''}>기타</option>
		    			</select>
		    			<select class="board_select" name="gubun3sel" id="gubun3sel">
		    				<option value="" ${board.gubun3 eq ''? 'selected': ''}>작업분류</option>
							<option value="T01" ${board.gubun3 eq 'T01'? 'selected': ''}>개발</option>
							<option value="T02" ${board.gubun3 eq 'T02'? 'selected': ''}>디자인</option>
							<option value="T03" ${board.gubun3 eq 'T03'? 'selected': ''}>퍼블리싱</option>
							<option value="T12" ${board.gubun3 eq 'T12'? 'selected': ''}>개발/디자인</option>
							<option value="T23" ${board.gubun3 eq 'T23'? 'selected': ''}>디자인/퍼블리싱</option>
							<option value="T13" ${board.gubun3 eq 'T13'? 'selected': ''}>개발/퍼블리싱</option>
							<option value="T77" ${board.gubun3 eq 'T77'? 'selected': ''}>개발/디자인/퍼블리싱</option>
							<option value="T99" ${board.gubun3 eq 'T99'? 'selected': ''}>기타</option>
		   				</select>
            </td>
          </tr>
          <tr>
            <th>글번호</th>
            <td>${board.idx }</td>
          </tr>
          <tr>
            <th class="point">팀이름 <span>*</span></th>
            <td><input type="text" name="groupNm" id="groupNm" value="${board.groupNm }"/></td>
          </tr>
          <tr>
            <th class="point">
							제목 <span>*</span>
							<label for="important" title="긴급일 경우에만 체크해주세요" style="margin-left: 7px;">
							<input type="checkbox" id="important" name="important" value="Y" ${board.important=='Y'?'checked':''}><span>긴급!</span>
							</label>
            </th>
            <td><input type="text" name="title" id="title" value="${board.title }"/></td>
          </tr>
          <tr>
            <th class="point">작성자 <span>*</span></th>
            <td><input type="text" class="gy" name="regName" value="${board.regName}" readonly="readonly"></td>
          </tr>
          <tr>
            <th class="point">희망요청일 <span>*</span></th>
            <td class="w_date">
              <fmt:formatDate var="reqDate" value="${board.endReqdate}" pattern="yyyy-MM-dd" timeZone="UTC"/>
              <input type="text" class="date" name="endReqdate" id="endReqdate" value="${reqDate }" ${board.status == "4"?"readonly":""}>
             <div style="display: none"> 
             	<fmt:formatDate var="regDate" value="${board.regDate}" pattern="yyyy-MM-dd" timeZone="UTC" />
              	<input type="date" class="date gy" name="regDate" value="${regDate}" > 
             </div>
            </td>
          </tr>
          <tr>
            <th>URL</th>
            <td>
              <input type="text" name="url" value="${board.url }">
            </td>
          </tr>
          <tr>
            <th class="point">내용 <span>*</span></th>
            <td class="cont">
            	<div id="contents">${board.contents}</div>
            </td>
          </tr>
          <tr>
            <th>첨부파일</th>
            <td class="file_input">
              <div class="fl_lef">
	              <input class="upload_name" id="itemUploadInput" value="${board.fileAttch.orgFileName}" disabled="disabled">
	              <label for="ufile" class="btn btn_file">파일찾기</label>
	              <input type="file" class="upload_hidden" name="ufile" id="ufile">
              </div>
              <div class="txt fs12 fl_lef">
               		파일 1개 이상일 경우 zip 파일로 올려주세요.<br>업로드 가능 파일 : <span>pdf,jpeg,jpg,png,gif,bmp,tiff,txt,doc,hwp,ppt,pptx,xlsx,xls,zip,7z,rar,alz</span>
              </div>
            </td>
          </tr>
          <tr>
            <c:if test="${user.member_group != 1 }">
	            <th>진행사항</th>
	            <td class="result" colspan="3">
	              <span class="${board.statusClass}">${board.statusNm}</span>
	            </td>
	            <input type="hidden" name="status" value="${board.status}"/>
	            <fmt:formatDate var="complDate2" value="${board.completeDate}" pattern="yyyy-MM-dd" timeZone="UTC"/>
	             <input type="hidden" name="completeDate" value="${complDate2}"/>
	        	</c:if>    
            
            <c:if test="${user.member_group == 1 }">
	            <th>접수</th>
	            <td class="receipt">
	              <input type="radio" id="st" name="status" value="1" ${board.status == 1 ? 'checked="checked"': ''}>
	              <label for="st">접수</label>
	              
	              <input type="radio" id="sc" name="status" value="2" ${board.status == 2 ? 'checked="checked"': ''}>
	              <label for="sc">접수완료</label>
	
	              <input type="radio" id="ig" name="status" value="3" ${board.status == 3 ? 'checked="checked"': ''}>
	              <label for="ig">작업중</label>
	
	              <input type="radio" id="mr" name="status" value="4" ${board.status == 4 ? 'checked="checked"': ''}>
	              <label for="mr">수정요청</label>
	
	              <input type="radio" id="df" name="status" value="5" ${board.status == 5 ? 'checked="checked"': ''}>
	              <label for="df">보류</label>
	
	              <input type="radio" id="ed" name="status" value="6" ${board.status == 6 ? 'checked="checked"': ''}>
	              <label for="ed">완료</label>
	
	              <span class="w_date">
	              	<fmt:formatDate var="complDate" value="${board.completeDate}" pattern="yyyy-MM-dd" timeZone="UTC"/>
	                <input type="text" class="date" id="completeDate" name="completeDate" value="${complDate}" readonly="readonly">
	                <span class="fs12">*작업완료시엔 완료 날짜를 작성해 주세요.</span>
	              </span>
	            </td>
            </c:if>
          </tr>
          <c:if test="${user.member_group == 1 }">
          <tr>
            <th>관리메모</th>
            <td>
							<textarea name="memo" class="w_memo">${board.memo }</textarea>
            </td>
          </tr>
					</c:if>
        </tbody>
      </table>
      <!-- //기본 게시판 상세보기 -->
			</form>

      <!-- 게시판 버튼 -->
      <div class="btn_wrap al_cen">
        <a href="javascript:void(0);" class="btn" id="submit-btn">수정</a>
        <a href="javascript:;" onclick="movePage('/board/${board.idx}',true)" class="btn btn1">취소</a>
      </div>
      <!-- //게시판 버튼 -->
    </div>
  </div>
  <!-- /#content -->
  <footer>
      <jsp:include page="/layout/footer.jsp"></jsp:include> 
  </footer>
</div>

</body>
<script type="text/javascript">
var submit_ck = true;

$(function(){
	$('#contents').summernote({
		  height: 300,                 // set editor height
		  width: '97%',
		  minHeight: null,             // set minimum height of editor
		  maxHeight: null,             // set maximum height of editor
		  focus: false,                  // set focus to editable area after initializing summernote
		  lang: 'ko-KR',
		  callbacks: {
			  onImageUpload: function(files, editor, welEditable) {
				var formData = new FormData();
				formData.append("file", files[0]);
				ajax.file('/upload/board/image', formData, function(data) { 
					if(data.result != 'ok') {
						alert(data.msg);
						return false;
					}
				    var node = document.createElement("IMG");
				    node.setAttribute("src", '/ui'+data.value);
					$('#contents').summernote('insertNode', node);
					console.log(data)
				})
			  }
		  }
		});
	 $('#endReqdate').datepicker({
			dateFormat: 'yy-mm-dd'
		});
	 $('#completeDate').datepicker({
			dateFormat: 'yy-mm-dd'
		});

	  var status = '${board.status}';
	  if(status == '6'){
		  $('#completeDate').attr('readonly',false);
	  }
	 
	  $('input[name=status]').on('change',function(){
		  var radioVal = $(this).val();
		  if(radioVal == '6'){
			  $('#completeDate').attr('readonly',false);
		  }else{
			  $('#completeDate').attr('readonly',true);
		  }
	  });
	 
		var file;
		
		$('input[name=ufile]').on('change', function(){
			var formData = new FormData();
			$($("#ufile")[0].files).each(function(index, file) {
				formData.append("ufile", file);
			});
			ajax.file('/upload/file', formData, function(data) { 
				if(data.result != 'ok') {
					alert(data.msg);
					return false;
				}
					file = data.value;
					$('#itemUploadInput').val(file.orgFileName);
					$.extend(file, {"sort": 0});
			})
		});
		
		
		$('#submit-btn').on('click', function(){
			var target1 = document.getElementById("gubun1sel");
			var target2 = document.getElementById("gubun2sel");
			var target3 = document.getElementById("gubun3sel");
			
			
			
			document.getElementsByName("gubun1")[0].value = target1.options[target1.selectedIndex].value+'@'+target1.options[target1.selectedIndex].text;
			document.getElementsByName("gubun2")[0].value = target2.options[target2.selectedIndex].value+'@'+target2.options[target2.selectedIndex].text;
			document.getElementsByName("gubun3")[0].value = target3.options[target3.selectedIndex].value+'@'+target3.options[target3.selectedIndex].text;
			
			

			

			var json = $('form[name=edit_form]').serializeObject();
			if(!$('#contents').summernote('isEmpty'))
				json.contents = $('#contents').summernote('code');
			else 
				json.contents = '';
			if(!empty(file))
				file.refTable = 'BOARD';
			json.fileAttch = file;

			if(empty($.trim(json.title))) {
				alert('제목을 입력해주세요.');
				focus('title');
				return;
			}else if(empty($.trim(json.contents))) {
				alert('내용을 입력해주세요.');
				focus('contents');
				return;
			}else if(empty($.trim(json.gubun1))) {
				alert('작업사이트를 선택해주세요.');
				focus('gubun1');
				return;
			}else if(empty($.trim(json.gubun2))) {
				alert('작업요청을 선택해주세요.');
				focus('gubun2');
				return;
			}else if(empty($.trim(json.gubun3))) {
				alert('작업구분을 선택해주세요.');
				focus('gubun3');
				return;
			}
			if(submit_ck){
				submit_ck = false;
				ajax.put('/board', json, function(data){
					if(data.result != 'ok') {
						alert(data.msg);
						return false;
					}else{
						alert('수정되었습니다.');
						movePage('/board/'+json.idx,true)
					}
				});
			}else{
				alert("처리중 입니다.");
			}

		});
	 })
</script>
</html>