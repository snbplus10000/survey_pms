<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>한국고용정보원</title>
	
	<!-- 공통 CSS -->
	<jsp:include page="/layout/core-css.jsp"/>
	
	<!-- 공통 JS -->
	<jsp:include page="/layout/core-js.jsp"/>
	
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
  <div id="wiset_wrap">
	<jsp:include page="/layout/header.jsp"></jsp:include> 
	  
	  <!-- Contents -->
	  <div id="content">
	    <div class="inner">
	      <!-- 상단 -->
	      <div class="list_top clearfix">
	        <p class="cont_title fl_lef">유지보수 접수</p>
	        <a href="javascript:popupOpen();"><span class="btn_howto">유지보수 접수 이용방법</span></a>
	        <a href="/pwInquiry"><span class="btn_howto">비밀번호 변경</span></a>
        	<c:if test="${user.member_group == 1}">
	       		<a href="javascript:excelDownload();"><span class="btn_howto">엑셀 다운로드</span></a>
		        <a href="javascript:filterList('order','is_new desc');"><span class="btn_howto">신규</span></a>
					</c:if>
					<div class="fl_rig">
						<select onchange="filterList('pageSize',this.value)">
							<option value="10" ${srchPage.pageSize eq '10'? 'selected': ''}>10개씩</option>
							<option value="20" ${srchPage.pageSize eq '20'? 'selected': ''}>20개씩</option>
							<option value="30" ${srchPage.pageSize eq '30'? 'selected': ''}>30개씩</option>
						</select> 
						<select id="cate" onchange="filterList('cate',this.value)">
							<option value="" ${srchPage.cate eq ''? 'selected': ''}>작업요청</option>	
		    				<option value="S01" ${srchPage.cate eq 'S01'? 'selected="selected"': ''}>추가요청</option>
		    				<option value="S02" ${srchPage.cate  eq 'S02'? 'selected="selected"': ''}>수정요청</option>
		    				<option value="S03" ${srchPage.cate  eq 'S03'? 'selected="selected"': ''}>삭제요청</option>
		    				<option value="S99" ${srchPage.cate  eq 'S99'? 'selected="selected"': ''}>기타</option>
						</select> 
						<select id="status" onchange="filterList('status',this.value)">
							<option value="" ${srchPage.status eq ''? 'selected': ''}>답변여부</option>
							<option value="1" ${srchPage.status eq '1'? 'selected': ''}>접수</option>
							<option value="2" ${srchPage.status eq '2'? 'selected': ''}>접수완료</option>
							<option value="3" ${srchPage.status eq '3'? 'selected': ''}>작업중</option>
							<option value="4" ${srchPage.status eq '4'? 'selected': ''}>수정요청</option>
							<option value="5" ${srchPage.status eq '5'? 'selected': ''}>보류</option>
							<option value="6" ${srchPage.status eq '6'? 'selected': ''}>완료</option>
						</select> 

					</div>
	      </div>
	      <!-- /.list_top -->
	      <!-- 리스트 테이블 -->
	      <table class="list_table">
	        <caption>기본 리스트 테이블</caption>
	        <colgroup>
						<col width="6%">
						<col width="10%">
						<col width="*">
						<col width="7%">
						<col width="11%">
						<col width="8%">
						<col width="8%">
						<c:if test="${user.member_group == 1}">
						<col width="12%">
						</c:if>
						<!-- <col width=""> -->
	        </colgroup>
	        <thead>
						<tr>
							<th>번호</th>
							<th>
								<c:choose>
									<c:when test="${srchPage.order eq 'gubun2 asc'}">
									<a href="javascript:;" onclick="filterList('order','gubun2 desc')">분류 <span class="arrow">▼</span></a>
									</c:when>
									<c:when test="${srchPage.order eq 'gubun2 desc'}">
									<a href="javascript:;" onclick="filterList('order','gubun2 asc')">분류 <span class="arrow">▲</span></a>
									</c:when>
									<c:otherwise>
									<a href="javascript:;" onclick="filterList('order','gubun2 asc')">분류</a>
									</c:otherwise>
								</c:choose>
							</th>
							<th>
								<c:choose>
									<c:when test="${srchPage.order eq 'title asc'}">
									<a href="javascript:;" onclick="filterList('order','title desc')">제목 <span class="arrow">▼</span></a>
									</c:when>
									<c:when test="${srchPage.order eq 'title desc'}">
									<a href="javascript:;" onclick="filterList('order','title asc')">제목 <span class="arrow">▲</span></a>
									</c:when>
									<c:otherwise>
									<a href="javascript:;" onclick="filterList('order','title asc')">제목</a>
									</c:otherwise>
								</c:choose>
							</th>
							<th>
								<c:choose>
									<c:when test="${srchPage.order eq 'reg_name asc'}">
									<a href="javascript:;" onclick="filterList('order','reg_name desc')">등록자 <span class="arrow">▼</span></a>
									</c:when>
									<c:when test="${srchPage.order eq 'reg_name desc'}">
									<a href="javascript:;" onclick="filterList('order','reg_name asc')">등록자 <span class="arrow">▲</span></a>
									</c:when>
									<c:otherwise>
									<a href="javascript:;" onclick="filterList('order','reg_name asc')">등록자</a>
									</c:otherwise>
								</c:choose>
							</th>
							<th>
								<c:choose>
									<c:when test="${srchPage.order eq 'reg_date asc'}">
									<a href="javascript:;" onclick="filterList('order','reg_date desc')">등록일 <span class="arrow">▼</span></a>
									</c:when>
									<c:when test="${srchPage.order eq 'reg_date desc'}">
									<a href="javascript:;" onclick="filterList('order','reg_date asc')">등록일 <span class="arrow">▲</span></a>
									</c:when>
									<c:otherwise>
									<a href="javascript:;" onclick="filterList('order','reg_date asc')">등록일</a>
									</c:otherwise>
								</c:choose>
							</th>
							<th>
								<c:choose>
									<c:when test="${srchPage.order eq 'end_reqdate asc'}">
									<a href="javascript:;" onclick="filterList('order','end_reqdate desc')">희망처리일 <span class="arrow">▼</span></a>
									</c:when>
									<c:when test="${srchPage.order eq 'end_reqdate desc'}">
									<a href="javascript:;" onclick="filterList('order','end_reqdate asc')">희망처리일 <span class="arrow">▲</span></a>
									</c:when>
									<c:otherwise>
									<a href="javascript:;" onclick="filterList('order','end_reqdate asc')">희망처리일</a>
									</c:otherwise>
								</c:choose>
							</th>
							<th>
								<c:choose>
									<c:when test="${srchPage.order eq 'status asc'}">
									<a href="javascript:;" onclick="filterList('order','status desc')">답변여부 <span class="arrow">▼</span></a>
									</c:when>
									<c:when test="${srchPage.order eq 'status desc'}">
									<a href="javascript:;" onclick="filterList('order','status asc')">답변여부 <span class="arrow">▲</span></a>
									</c:when>
									<c:otherwise>
									<a href="javascript:;" onclick="filterList('order','status asc')">답변여부</a>
									</c:otherwise>
								</c:choose>
							</th>
							<c:if test="${user.member_group == 1}">
							<th>
								<c:choose>
									<c:when test="${srchPage.order eq 'gubun4 asc'}">
									<a href="javascript:;" onclick="filterList('order','gubun4 desc')">작업분류 <span class="arrow">▼</span></a>
									</c:when>
									<c:when test="${srchPage.order eq 'gubun4 desc'}">
									<a href="javascript:;" onclick="filterList('order','gubun4 asc')">작업분류 <span class="arrow">▲</span></a>
									</c:when>
									<c:otherwise>
									<a href="javascript:;" onclick="filterList('order','gubun4 asc')">작업분류</a>
									</c:otherwise>
								</c:choose>
							</th>
							</c:if>
						</tr>
	        </thead>
	        <tbody>
						<c:forEach items="${board_list}" var="board" varStatus="status">
							<tr>
								<td class="num">${board.idx }</td>
								<td class="category">${board.gubun2Nm }</td>
								<td class="title">
									<c:if test="${board.important == 'Y' }">
									<span style="color: red; font-weight:bold; float:left; padding-right: 6px;">(긴급)</span>
									</c:if>
									<span class="team">${board.groupNm}</span>
									<a href="javascript:;" onclick="movePage('/board/${board.idx}', true)">
										${board.title }
									</a>
									<span class="coment">${board.replyCnt }</span> 
									<c:if test="${board.isFile == 1 }">
										<span class="file"><img src="/public/img/icon_file.png" alt="file"></span> 
									</c:if>
									<c:if test="${board.isNew == 1}">
										<span class="new">NEW</span>
									</c:if>
								<td class="writer">${board.regName}</td>
								<td class="date"><fmt:formatDate value="${board.regDate}" type="date" pattern="yy-MM-dd HH:mm" timeZone="UTC" /></td>
								<td class="date"><fmt:formatDate value="${board.endReqdate}" type="date" pattern="yy-MM-dd" timeZone="UTC" /></td>
								<td class="result"><span
									class="${board.statusClass}">${board.statusNm }</span>
								</td>
								<c:if test="${user.member_group == 1}">
								<td class="date">${board.gubun4Nm }</td>
								</c:if>
							</tr>
						</c:forEach>
	        </tbody>
	      </table>
	      <!-- /.list-table -->
	
	      <!-- 버튼 -->
	      <div class="btn_wrap al_right">
	        <button type="button" class="btn" onclick="writePage()">글쓰기</button>
	      </div>
			
	      <!-- 페이징 -->
				<div class="list_paging">
					<ul class="pagination">
						<!--  <li class="first"><a href="#">처음</a></li> -->
						<li class="prve">
							<c:choose>
								<c:when test="${page.groupNo == 1}">
									<a href="javascript:void(0);">이전</a>
								</c:when>
								<c:otherwise>
									<a href="javascript:;" onclick="pageList(${page.prevPageNo})">이전</a>
								</c:otherwise>
							</c:choose>
						</li>
						<c:forEach varStatus="status" begin="${page.pageSno }" end="${page.pageEno }">
							<li class="page ${page.pageNo == status.index?'active':'' }">
								<a href="javascript:;" onclick="pageList(${status.index})">${status.index }</a>
							</li>
						</c:forEach>
						<li class="next">
							<c:choose>
								<c:when test="${page.groupNo >= page.totalGroup}">
									<a href="javascript:void(0);">다음</a>
								</c:when>
								<c:otherwise>
									<a href="javascript:;" onclick="pageList(${page.nextPageNo})">다음</a>
								</c:otherwise>
							</c:choose>
						</li>
						<!-- <li class="last"><a href="#">마지막</a></li> -->
					</ul>
				</div>
	      <!-- /.list_paging -->
	      <!-- 검색 -->
	      <div class="search">
	        <select id="srchCode" name="srchCode">
        		<option value="total" ${srchPage.srchCode eq 'total'? 'selected': ''}>전체</option>
        		<option value="writer" ${srchPage.srchCode eq 'writer'? 'selected': ''}>등록자</option>
        		<option value="title" ${srchPage.srchCode eq 'title'? 'selected': ''}>제목</option>
	          <option value="group" ${srchPage.srchCode eq 'group'? 'selected': ''}>팀 이름</option>
        		<option value="contents" ${srchPage.srchCode eq 'contents'? 'selected': ''}>본문</option>
	        </select>
	        <input type="text" id="srchValue" name="srchValue" value="${srchPage.srchValue }" onkeydown="if(event.keyCode == 13){search();}">
	        <button type="button" name="button" onclick="search()">검색</button>
	      </div>
        <!-- /.search -->
	
	    </div>
	
	  </div>
	  <!-- /#content -->
	  
	  <footer>
        <jsp:include page="/layout/footer.jsp"></jsp:include>   
      </footer>
   </div>
</body>

<script type="text/javascript">

	function popupOpen(){
		var popUrl = "../public/pop.html";	//팝업창에 출력될 페이지 URL
		var popOption = "width=730, height=560, resizable=no, scrollbars=no, status=no;";    //팝업창 옵션(optoin)
			window.open(popUrl,"",popOption);
	}
	
	var params = $.getParameters();
	
	function filterList(key, value) {
		params.page = 1;
		var queryStr = '';
		params[key] = value;
		queryStr = $.param(params);

		location.href = "/board/list?" + encodeURI(decodeURI(queryStr));
	}

	function pageList(page) {
		var queryStr = '';
		params.page = page;
		queryStr = $.param(params);
		
		location.href = "/board/list?" + encodeURI(decodeURI(queryStr));
	}
	
	function search() {
		params.page = 1;
		var srchCode = $('#srchCode').val();
		var srchValue = $('#srchValue').val();
		var queryStr = '';
		/* if(empty(srchValue)) {
			alert('검색어를 입력해주세요.');
			return;
		} */
		params.srchCode = srchCode;
		params.srchValue = srchValue;
		queryStr = $.param(params);
		
		location.href = "/board/list?" + encodeURI(decodeURI(queryStr));
	}
	
	function writePage() {
		location.href = "/board/new";
	}
	
	function excelDownload(){
		var srchCode = $('#srchCode').val();
		var srchValue = $('#srchValue').val();
		var cate = $('#cate').val();
		var status = $('#status').val();
		var dept = $('#dept').val();
		var queryStr = '';
		
		params.srchCode = srchCode;
		params.srchValue = srchValue;
		params.cate = cate;
		params.status = status;
		params.dept = dept;
		
		queryStr = $.param(params);
		location.href = "/board/excel?" + encodeURI(decodeURI(queryStr));
	}
	
</script>
</html>