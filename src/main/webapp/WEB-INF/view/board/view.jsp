<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%> 
<%@ page isELIgnored="false" %>
<!DOCTYPE html>
<html lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
<title>한국고용정보원</title>
	
	<!-- 공통 CSS -->
	<jsp:include page="/layout/core-css.jsp"/>
	
	<!-- 공통 JS -->
	<jsp:include page="/layout/core-js.jsp"/>
	
	
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<div id="wiset_wrap"> 
	<jsp:include page="/layout/header.jsp"></jsp:include> 
  
  <!-- Contents -->
  <div id="content">
    <div class="inner">
      <!-- 상단 -->
      <div class="list_top clearfix">
        <p class="cont_title fl_lef">유지보수 접수</p>
      </div>
      <!-- /.list_top -->
      <!-- 기본 게시판 상세보기 -->
      <table class="list_detail">
        <colgroup>
          <col width="12%">
          <col width="38%">
          <col width="12%">
          <col width="38%">
        </colgroup>
        <tbody>
          <tr>
            <th>구분</th>
            <td colspan="3">
              <ul>
                <li>작업사이트 : <span>${board.gubun1Nm }</span></li>
                <li>작업 요청 : <span>${board.gubun2Nm }</span></li>
                <li>작업 구분 : <span>${board.gubun3Nm }</span></li>
                <c:if test="${user.member_group eq '1'}">
                	<li>작업 분류 : <span>${board.gubun4Nm }</span></li>
                </c:if>
              </ul>
            </td>
          </tr>
          <tr>
            <th>글번호</th>
            <td colspan="3">${board.idx }</td>
          </tr>
          <tr>
            <th>팀이름</th>
            <td colspan="3">${board.groupNm }</td>
          </tr>
          <tr>
            <th>제목</th>
            <td colspan="3">
	          	<c:if test="${board.important == 'Y' }">
							<span style="color: red; font-weight:bold; float:left; padding-right: 6px;">(긴급)</span>
	          	</c:if>
							${board.title }
            </td>
          </tr>
          <tr>
            <th>작성자</th>
            <td colspan="3">
              <ul class="info">
                <li>${board.regName }</li>
                <li>연락처 : <span>${board.tel }</span></li>
              </ul>
            </td>
          </tr>
          <tr>
            <th>참조</th>
            <td colspan="3">${board.refEmail }</td>
          </tr>
          <tr>
            <th>희망요청일</th>
            <td><fmt:formatDate value="${board.endReqdate}" pattern="yyyy-MM-dd" timeZone="UTC"/></td>
            <th>작성일</th>
            <td><fmt:formatDate value="${board.regDate}" pattern="yyyy-MM-dd HH:mm" timeZone="UTC"/></td>
          </tr>
          <tr>
            <th>URL</th>
            <td colspan="3">
              <a href="${board.url}" target="_blank">${board.url}</a>
            </td>
          </tr>
          <tr>
            <th>내용</th>
            <td class="cont" colspan="3">
            	${board.contents }
            </td>
          </tr>
          <tr>
            <th>첨부파일</th>
            <td colspan="3">
              <i class="file"></i>
              <a href="/file/down/${board.fileAttch.id }">${board.fileAttch.orgFileName }</a>
            </td>
          </tr>
          <tr>
            <th>진행사항</th>
            <td class="result" colspan="3">
              <span class="${board.statusClass}">${board.statusNm}</span>
              <c:if test="${board.status == 6 }">
	              <div class="date">
	               	 완료일 : <fmt:formatDate value="${board.completeDate}" pattern="yyyy-MM-dd" timeZone="UTC"/>
	              </div>
              </c:if>
            </td>
          </tr>
          <c:if test="${user.member_group == 1}">
          <tr>
            <th>관리메모</th>
            <td class="result" colspan="3">
             	${board.memo }
            </td>
          </tr>
          </c:if>
        </tbody>
      </table>
      <!-- //기본 게시판 상세보기 -->
      
      <div class="co_writer">
        <div class="edit_wrap">
          <button type="button" id="updateReqBtn" class="edit_btn">수정요청</button>
          <span>※ 수정요청시 버튼을 눌러주세요 / 작업요청 컨펌 완료시에는 완료되었다고 댓글 남겨주세요</span>
        </div>
        <div class="reply_txt">
	        <form name="replyForm">
	        	<input type="hidden" name="isUpdateReq" value="0"/>
	        	<input type="hidden" name="boardTitle" value="${board.title }"/>
	        	<input type="hidden" name="regEmail" value="${board.regEmail }"/>
	        	<input type="hidden" name="refEmail" value="${board.refEmail }"/>
						<input type="hidden" name="refIdx" value="${board.idx }"/>
	          <textarea class="comment" name="comment" style="resize:none;"></textarea>
	         </form>
          <button type="button" id="reply_submit">등록</button>
        </div>
        <div class="file_input">
          <div class="fl_lef">
            <input class="upload_name" id="itemUploadInput" value="" disabled="disabled">
            <label for="ufile" class="btn btn_file">파일찾기</label>
						<input id="ufile" name="ufile" type="file" class="upload_hidden"/>
          </div>
          <div class="txt fs12 fl_lef">
            	파일 1개 이상일 경우 zip 파일로 올려주세요.<br>업로드 가능 파일 : <span>pdf,jpeg,jpg,png,gif,bmp,tiff,txt,doc,hwp,ppt,pptx,xlsx,xls,zip,7z,rar,alz</span>
          </div>
        </div>
      </div>

      <div class="list_reply">
        <p class="title">댓글목록</p>
        <!-- 답글 -->
        <div class="reply_wrap">
					<c:forEach items="${board.reply }" var="re">
          <div class="box ${re.memberGroup == 1 ? 'admin' : ''}">
            <div class="re_top">
              <span class="name ${re.memberGroup == 1 ? 'admin' : ''}">${re.name }</span>
              <span class="date"><fmt:formatDate value="${re.regDate}" pattern="yyyy-MM-dd HH:mm" timeZone="UTC"/></span>
              <!-- <ul class="edit">
                <li><a href="#">수정</a></li>
                <li><a href="#">삭제</a></li>
              </ul> -->
            </div>
            <div class="re_view">
              ${re.comment }
            </div>
            <c:if test="${re.fileId != null }">
            <div class="re_file">
              <a href="/file/down/${re.fileId }">${re.fileName }</a>
            </div>
            </c:if>
          </div>
          </c:forEach>
        </div>
        <!-- /.reply_wrap -->
      </div>
      
      <!-- 게시판 버튼 -->
      <div class="btn_wrap al_cen">
			<c:if test="${user.id eq board.regId && user.member_group != 1}">
				<c:if test="${board.status == 1}">
				<a href="javascript:;" onclick="movePage('/board/edit/${board.idx}',true)" class="btn">수정</a>
				<a href="javascript:void(0);" class="btn btn1 delete-btn">삭제</a>
				</c:if>
			</c:if>
		  <c:if test="${user.member_group == 1}">
				<a href="javascript:;" onclick="movePage('/board/edit/${board.idx}',true)" class="btn">수정</a>
				<a href="javascript:void(0);" class="btn btn1 delete-btn">삭제</a>
 		  </c:if>
          <a href="javascript:;" onclick="movePage('/board/list',true)" class="btn btn1">목록</a>
      </div>
      <!-- //게시판 버튼 -->
    </div>
  </div>
  <!-- /#content -->
  
	<footer>
       <jsp:include page="/layout/footer.jsp"></jsp:include>
    </footer>
</div>    	
</body>
<script type="text/javascript">
var submit_ck = false;
 $(function(){
		$('#updateReqBtn').on('click', function() {
			if($(this).hasClass('active')) {
				$(this).removeClass('active');
				$('input[name=isUpdateReq]').val(0);
			} else {
				$(this).addClass('active');
				$('input[name=isUpdateReq]').val(1);
			}
		});
		
	   $('.delete-btn').on('click', function() {
			if(confirm("정말 삭제하시겠습니까?")) {
				ajax.get('/board/del/${board.idx}', function(data){
					if(data.result != 'ok'){
						alert(data.msg);
						return false;
					}else{
						alert(data.msg);
						location.href='/board/list';
					}
				});
			}
		});

		var file;

	 	$('input[name=ufile]').on('change', function(){
	 		var formData = new FormData();
	 		$($("#ufile")[0].files).each(function(index, file) {
	 			formData.append("ufile", file);
	 		});
	 		ajax.file('/upload/file', formData, function(data) { 
	 			if(data.result != 'ok') {
	 				alert(data.msg);
	 				return false;
	 			}
	 				file = data.value;
	 				$('#itemUploadInput').val(file.orgFileName);
	 				$.extend(file, {"sort": 0});
	 		})
	 	});
	 	
	   $('#reply_submit').on('click', function(){
		   if(submit_ck) {
			   alert('처리중 입니다.');
			   return;
		   }
		   submit_ck = true;
		   var json = $('form[name=replyForm]').serializeObject();
		   if(!empty(file))
			   file.refTable = 'REPLY';
		   json.fileAttch = file;
		   
			var jsonStr = JSON.stringify(json);
			
			$.ajax({
				type: "POST",
				url: "/board/reply",
				data: jsonStr,
				datatype : "JSON",
				contentType: "application/json",
				success: function(data) {
					if(data.result != "ok") {
						alert(data.msg);
						return;
					}
					location.reload();
				},
				error: function(xhr, status, error) {
					alert(status);
				},
				complete: function() {
					//submit_ck = false;
				}
			});	 
		   
	   });
 })
</script>
</html>