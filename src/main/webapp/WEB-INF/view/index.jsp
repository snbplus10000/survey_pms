<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="util" uri="conf/tags.tld"%>
<!DOCTYPE html>
<html lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
 <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>한국고용정보원</title> 
    
  	<!-- 공통 CSS -->
		<jsp:include page="/layout/core-css.jsp"/>
 	
 	<!-- 공통 JS -->
		<jsp:include page="/layout/core-js.jsp"/>
    
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    
</head>
<body>
    
<div id="wiset_wrap">

  <div id="header">
    <div class="inner">
      <div class="fl logo"><a href=""><img src="/public/img/logo.gif" style="height:50px" alt="로고"></a></div> 
      <div class="rig">
        <ul class="site">
          <li>
            <span class="tit">관리자</span>
			<ul>
				<li>
					<a href="https://koms.or.kr/" class='before'>홈페이지</a> 
				</li>
			</ul>
          </li>

          <li>
            <span class="tit">사이트</span>
           	<ul>
				<li>
				<a href="https://koms.or.kr/" class='before'>홈페이지</a> 
				</li>
			</ul>
          </li>
        </ul>
      </div>
    </div>
  </div> 
  
  <!-- Contents -->
  <div id="content">
    <div class="inner">
      <table class="login_table">
        <tr>
          <td>
            <div class="inner">
              <p class="tit">로그인</p>
              <!-- 로그인폼 -->
              <div class="input_wrap">
                <form method="post" action="/login">
	                <div class="lef">
	                  <input type="text" name="id" id="id" placeholder="아이디">
	                  <input type="password" name="pw" id="pw" placeholder="비밀번호">
	                </div>
	                <div class="rig">
	                  <button type="submit" name="button">로그인</button>
	                </div>
                </form>
              </div>
              <!-- /.input_wrap -->
            </div>
           <!--  <input type="checkbox" id="idsave"><label for="idsave">아이디 저장</label> -->
          </td>
        </tr>
      </table>
      <!-- /.login_table -->
    </div>
  </div>
  <!-- /#content -->
   
  <footer>
     <jsp:include page="/layout/footer.jsp"></jsp:include>
  </footer>
</div>
</body>
<script type="text/javascript">
	var msg = "${msg}";
	if (msg == "no") {
		alert("아이디 혹은 비밀번호가 맞지 않습니다.");
	}
</script>
</html>