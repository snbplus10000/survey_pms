<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="util" uri="conf/tags.tld"%>
<!DOCTYPE html>
<html lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
 <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>koms-비밀번호 변경</title> 
    
  	<!-- 공통 CSS -->
		<jsp:include page="/layout/core-css.jsp"/>
 	
 	<!-- 공통 JS -->
		<jsp:include page="/layout/core-js.jsp"/>
    
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script>
	    $(function(){
	
	    	$('#btnUpdate').click(function() { 
	    		
	    		
	    		
	    		if($("#userPw").val() == "")
	    		{
	    			alert("비밀번호를 입력하세요");
	    			return false;
	    		}
	    		
	    		if($("#userNewPw").val() == "")
	    		{
	    			alert("새 비밀번호를 입력하세요");    			
	    			return false;
	    		}
	    		
	    		if($("#userNewPwRe").val() != $("#userNewPw").val())
	    		{
	    			alert("새로운 비밀번호가 서로 상이합니다");    			
	    			return false;
	    		}
	    		
	    		if($("#userNewPw").val().length < 8)
	    		{
	    			alert("새 비밀번호는 8자 이상입니다");
	    			return false;
	    		}
	    		
	    		if($("#userEmail").val() == "")
	    		{
	    			alert("이메일을 입력하세요");
	    			return false;
	    		}
	    		if(!emailcheck($("#userEmail").val()))
	    		{
	    			return false;
	    		}
	    		
	    		$('#pwFrm').submit();
	    	});
	    });
    
    </script>
</head>
<body>
<div id="wiset_wrap">
	<jsp:include page="/layout/header.jsp"></jsp:include> 
  		
  		
  		<form id="pwFrm" method="post" action="/pwChange">
  			<input type="hidden" name="userId" id="userId" value="${userInfo.id}">
  			<input type="hidden" name="userEmail" id="userEmail" value="${userInfo.email}">
	  	  <!-- Contents -->
		  <div id="content">
		    <div class="inner">
		      <!-- 기본 게시판 상세보기 -->
		      <table class="login_table login_table2">
		        <tr>
		          <td>
		            <p class="tit">비밀번호 변경</p>
		            <table class="list_detail pw_table">
		              <caption>비밀번호 변경</caption>
		              <colgroup>
		                <col width="35%">
		                <col width="%">
		              </colgroup>
		              <tbody>
		                <tr>
		                  <th>기존 비밀번호</th>
		                  <td>
		                    <input type="password" name="userPw" id="userPw" />
		                  </td>
		                </tr>
		                <tr>
		                  <th>변경 비밀번호</th>
		                  <td>
		                    <input type="password" name="userNewPw" id="userNewPw" />
		                  </td>
		                </tr>
		                <tr>
		                  <th>변경 비밀번호 재입력</th>
		                  <td>
		                    <input type="password" name="userNewPwRe" id="userNewPwRe" />
		                  </td>
		                </tr>
		              </tbody>
		            </table>
		            <!-- //기본 게시판 상세보기 -->
		            <!-- 버튼 -->
		            <div class="btn_wrap al_cen">
		              <button type="submit" id="btnUpdate" class="btn" style="width:150px;">비밀번호 변경</button>
		            </div>
		
		          </td>
		        </tr>
		      </table>
		
		    </div>
		  </div>
		  <!-- /#content -->
		  </form>
		  
	  <footer>
        	<jsp:include page="/layout/footer.jsp"></jsp:include> 
      </footer>
</div>
</body>
<script type="text/javascript">
	var msg = "${msg}";
	if (msg == "MSG_NOT_DATA") 
	{
		alert("비밀번호를 확인하세요");
	}
	
	function emailcheck(strValue)
	{

		var regExp = /[0-9a-zA-Z][_0-9a-zA-Z-]*@[_0-9a-zA-Z-]+(\.[_0-9a-zA-Z-]+){1,2}$/;
	
		if(strValue.lenght == 0)
		{
			return false;
		}

		if(!strValue.match(regExp))
		{
			return false;
		}
	
		return true;

	}

</script>
</html>